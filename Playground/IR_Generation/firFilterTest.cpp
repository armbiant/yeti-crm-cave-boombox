#include <algorithm>
#include <chrono>
#include <cstdlib>
#include <cstring>
#include <eigen3/Eigen/Dense>
#include <iostream>
#include <sndfile.h>
#include <vector>

static const unsigned int numtrials = 10;

using ChronoResolution = std::chrono::milliseconds;

SNDFILE* infile;

void clean_up_and_exit()
{
    sf_close(infile);
    exit(1);
}

int main()
{
    SF_INFO sfinfo;
    sf_count_t count;

    memset(&sfinfo, 0, sizeof(sfinfo));
    if ((infile = sf_open(std::string(DATADIR + std::string("irKernels.wav")).c_str(), SFM_READ, &sfinfo)) == NULL)
    {
        std::cerr << "Error : Not able to open input file" << std::endl;
        clean_up_and_exit();
    }

    int numBands = sfinfo.channels;
    int bandLen = sfinfo.frames;
    int fs = sfinfo.samplerate;

    std::cout << "Number of frequency bands: " << numBands << std::endl;
    std::cout << "Filter kernel size: " << bandLen << std::endl;
    std::cout << "Sample rate: " << fs << std::endl;

    std::vector<float> wavSamples(bandLen * numBands, 0);
    std::vector<std::vector<float>> freqBands(numBands, std::vector<float>(bandLen, 0));

    count = sf_readf_float(infile, wavSamples.data(), bandLen * numBands);
    if (count <= 0)
    {
        std::cerr << "No Samples in ir file" << std::endl;
        clean_up_and_exit();
    }

    Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> filterBands(numBands, bandLen);
    for (int i = 0; i < numBands; ++i)
    {
        for (int j = 0; j < bandLen; ++j)
            filterBands(i, j) = wavSamples[numBands * i + j];
    }

    std::vector<int> numPaths{10000, 25000, 50000, 100000};
    std::vector<int> irLen{fs / 2, fs, 2 * fs};
    std::vector<int> numChannels{1};

    for (auto nPaths : numPaths)
    {
        for (auto iLen : irLen)
        {
            for (auto nChan : numChannels)
            {
                std::cout << "Number of Paths: " << nPaths << std::endl;
                std::cout << "Length of IR: " << iLen << std::endl;
                std::cout << "Number of channels: " << nChan << std::endl;

                double totalTimeElapsed_us = 0;
                for (int trial = 0; trial < numtrials; ++trial)
                {
                    std::vector<int> pathArrival(nPaths, 0);
                    Eigen::ArrayXXf pathDirAmp(nPaths, nChan);
                    Eigen::ArrayXXf pathReflAmp(nPaths, numBands);
                    std::vector<std::vector<float>> ir(nChan, std::vector<float>(iLen, 0));

                    // Generate random paths
                    for (int i = 0; i < nPaths; ++i)
                    {
                        pathArrival[i] = rand() % (iLen - bandLen) + bandLen / 2;
                        for (int j = 0; j < nChan; ++j)
                            pathDirAmp(i, j) = static_cast<float>(rand()) / static_cast<float>(RAND_MAX);
                        for (int j = 0; j < numBands; ++j)
                            pathReflAmp(i, j) = static_cast<float>(rand()) / static_cast<float>(RAND_MAX);
                    }

                    auto start = std::chrono::high_resolution_clock::now();
                    // Generate IR
                    for (int chan = 0; chan < nChan; ++chan)
                    {
                        for (int path = 0; path < nPaths; ++path)
                        {
                            auto pathGain = pathDirAmp(path, chan) * pathReflAmp.row(path);
                            Eigen::Map<Eigen::ArrayXf> irMap(ir[chan].data() + pathArrival[path] - bandLen / 2, bandLen);
                            irMap += (pathGain.matrix() * filterBands).array();
                        }
                    }
                    auto stop = std::chrono::high_resolution_clock::now();
                    auto elapsed = std::chrono::duration_cast<ChronoResolution>(stop - start);
                    totalTimeElapsed_us += static_cast<double>(elapsed.count());
                }
                std::cout << "\tTotal time elapsed: " << totalTimeElapsed_us << " milliseconds" << std::endl;
                std::cout << "\tAverage time per trial: " << totalTimeElapsed_us / numtrials << " milliseconds" << std::endl;
            }
        }
    }
    return 0;
}