#include <algorithm>
#include <chrono>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iostream>
#include <vector>

#include <Eigen/Dense>
#include <jsoncpp/json/json.h>

static const unsigned int numtrials = 10;

using ChronoResolution = std::chrono::milliseconds;

class SOS
{
  private:
    Eigen::ArrayXf b0_, b1_, b2_;
    Eigen::ArrayXf a0_, a1_, a2_;
    Eigen::ArrayXf s1_, s2_;

    int numParallelSections_{0};

  public:
    SOS(const Eigen::ArrayXX<float>& coeffs)
        : numParallelSections_(coeffs.rows())
    {
        if (coeffs.cols() != 6)
            return;

        b0_ = coeffs.col(0);
        b1_ = coeffs.col(1);
        b2_ = coeffs.col(2);
        a0_ = coeffs.col(3);
        a1_ = coeffs.col(4);
        a2_ = coeffs.col(5);

        s1_.resize(numParallelSections_);
        s2_.resize(numParallelSections_);

        clearStates();
    }

    void process(Eigen::Ref<Eigen::ArrayX<float>> inFrame)
    {
        Eigen::ArrayX<float> temp = inFrame;
        inFrame = inFrame * b0_ + s1_;
        s1_ = temp * b1_ - inFrame * a1_ + s2_;
        s2_ = temp * b2_ - inFrame * a2_;
    }

    void processBlock(Eigen::Ref<Eigen::Array<float, -1, -1, Eigen::ColMajor>> inBlock)
    {
        for (size_t i = 0; i < inBlock.cols(); ++i)
            process(inBlock.col(i));
    }

    void clearStates()
    {
        s1_.setZero();
        s2_.setZero();
    }
};

class SOS_Cascade
{
  private:
    std::vector<SOS> sosCasc_;
    int numInputs_{0};

  public:
    SOS_Cascade(const Eigen::ArrayXX<float>& coeffs)
        : numInputs_(coeffs.rows())
    {
        if (coeffs.cols() % 6 != 0)
            return;

        for (size_t i = 0; i < coeffs.cols(); i += 6)
        {
            SOS temp(coeffs.block(0, i, coeffs.rows(), 6));
            sosCasc_.push_back(temp);
        }
    }

    void process(Eigen::Ref<Eigen::ArrayX<float>> inFrame)
    {
        for (auto& sos : sosCasc_)
            sos.process(inFrame);
    }

    void processBlock(Eigen::Ref<Eigen::Array<float, -1, -1, Eigen::ColMajor>> inBlock)
    {
        for (auto& sos : sosCasc_)
            sos.processBlock(inBlock);
    }

    void clearStates()
    {
        for (auto& sos : sosCasc_)
            sos.clearStates();
    }

    int getNumInputs() { return numInputs_; }
};

class LRTree
{
  private:
    std::vector<SOS_Cascade> filterStages_;
    int numStages_{0};

  public:
    LRTree(const std::string filename)
    {
        std::ifstream file;
        file.open(filename.c_str());
        std::string input((std::istreambuf_iterator<char>(file)), (std::istreambuf_iterator<char>()));
        file.close();

        if (input.empty())
            return;

        Json::Value deserializeRoot;
        Json::Reader reader;

        if (!reader.parse(input, deserializeRoot))
            return;

        numStages_ = deserializeRoot.get("NumStages", 0).asInt();
        Json::Value sosCoeffs = deserializeRoot.get("SOS", 0);

        for (auto& sosStage : sosCoeffs)
        {
            if (sosStage.empty())
                break;
            auto numFilters = sosStage.size();
            auto numCoeffs = sosStage.begin()->size();
            Eigen::ArrayXX<float> sosCoeffs(numFilters, numCoeffs);
            for (Json::ArrayIndex i = 0; i < numFilters; ++i)
            {
                for (Json::ArrayIndex j = 0; j < numCoeffs; ++j)
                    sosCoeffs(i, j) = sosStage[i][j].asFloat();
            }
            filterStages_.push_back(SOS_Cascade(sosCoeffs));
        }
    }

    void processBlock(Eigen::Ref<Eigen::Array<float, -1, -1, Eigen::ColMajor>> inBlock)
    {
        for (size_t frame = 0; frame < inBlock.cols(); ++frame)
        {
            for (size_t stage = 0; stage < numStages_; ++stage)
            {
                filterStages_[stage].process(inBlock.block(0, frame, 1 << (numStages_ - stage), 1));
                for (size_t j = 0; j < (1 << (numStages_ - stage - 1)); ++j)
                    inBlock(j, frame) = inBlock((1 << (stage + 1)) * j, frame) + inBlock((1 << (stage + 1)) * j + 1, frame);
            }
        }
    }

    void resetFilters()
    {
        for (auto& f : filterStages_)
            f.clearStates();
    }

    int getNumInputs()
    {
        if (!filterStages_.empty())
            return filterStages_.begin()->getNumInputs();
        else
            return 0;
    }
};

int main()
{
    int fs = 48000;

    LRTree filter(DATADIR + std::string("IIR_SOSCoefficients.json"));

    std::vector<int> numPaths{10000, 50000, 100000};
    std::vector<int> irLen{fs / 2, fs, 2 * fs};
    std::vector<int> numChannels{1};
    int numBands = filter.getNumInputs();

    for (auto nPaths : numPaths)
    {
        for (auto iLen : irLen)
        {
            for (auto nChan : numChannels)
            {
                std::cout << "Number of Paths: " << nPaths << std::endl;
                std::cout << "Length of IR: " << iLen << std::endl;
                std::cout << "Number of channels: " << nChan << std::endl;

                double totalTimeElapsed_us = 0;
                double impTrainGenTime_us = 0;
                for (int trial = 0; trial < numtrials; ++trial)
                {
                    filter.resetFilters();

                    std::vector<int> pathArrival(nPaths, 0);
                    Eigen::ArrayXX<float> pathDirAmp(nChan, nPaths);
                    Eigen::ArrayXX<float> pathReflAmp(numBands, nPaths);

                    // Generate random paths
                    for (int path = 0; path < nPaths; ++path)
                    {
                        pathArrival[path] = rand() % iLen;
                        for (int chan = 0; chan < nChan; ++chan)
                            pathDirAmp(chan, path) = static_cast<float>(rand()) / static_cast<float>(RAND_MAX);
                        for (int band = 0; band < numBands; ++band)
                            pathReflAmp(band, path) = static_cast<float>(rand()) / static_cast<float>(RAND_MAX);
                    }
                    std::sort(pathArrival.begin(), pathArrival.end());

                    auto start = std::chrono::high_resolution_clock::now();

                    for (int chan = 0; chan < nChan; ++chan)
                    {
                        // Generate impulse train
                        auto startImp = std::chrono::high_resolution_clock::now();
                        Eigen::Array<float, -1, -1, Eigen::ColMajor> iTrainBands(numBands, iLen);
                        iTrainBands.setZero();
                        for (int path = 0; path < nPaths; ++path)
                        {
                            iTrainBands.col(pathArrival[path]) += pathDirAmp(chan, path) * pathReflAmp.col(path);
                        }

                        auto stopImp = std::chrono::high_resolution_clock::now();
                        auto elapsedImp = std::chrono::duration_cast<ChronoResolution>(stopImp - startImp);
                        impTrainGenTime_us += static_cast<double>(elapsedImp.count());

                        filter.processBlock(iTrainBands);
                        std::vector<float> irChan(iTrainBands.row(0).begin(), iTrainBands.row(0).end());
                    }

                    auto stop = std::chrono::high_resolution_clock::now();
                    auto elapsed = std::chrono::duration_cast<ChronoResolution>(stop - start);
                    totalTimeElapsed_us += static_cast<double>(elapsed.count());
                }
                std::cout << "\tTotal time elapsed: " << totalTimeElapsed_us << " milliseconds" << std::endl;
                std::cout << "\tAverage time per trial: " << totalTimeElapsed_us / numtrials << " milliseconds" << std::endl;
                std::cout << "\tTime per path (impulse train generation): " << impTrainGenTime_us / (nChan * numtrials) << " milliseconds" << std::endl;
            }
        }
    }
    return 0;
}