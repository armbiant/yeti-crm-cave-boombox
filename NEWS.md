vaRays release notes
===================
Here you will find a high level list of new features and bugfixes for each releases. 

vaRays 0.4.0 (2020-11-04)
---------------------------
This is an official release in the 0.4 stable series.

* ♻️ Major refactoring of the ray tracing and the way Embree is handled
* ✅ Added new ray tracing tests

vaRays 0.3.2 (2020-09-21)
---------------------------
This is an official release in the 0.3 stable series.

* 🍱 Added vaRays logo and graphic charter
* ♿️ New vaRays Python application written using pyvarays
* ✅ Convolver test now checks random IR updates of different lengths

vaRays 0.3.0 (2020-09-09)
---------------------------
This is an official release in the 0.3 stable series.

New Features:
* ✨ Added Doppler shift for direct sounds in live auralization
* ✨ Added Python wrapper

Core:
* ➕ New and improved logger using [spdlog](https://github.com/gabime/spdlog)
* ⚡️🏗 Improved ambisonic IR computation speed using Linkwitz-Riley filter tree in `VaraysEncoder`
* 🚚♻️ Renamed RayConvolver to VaraysAuralizer and refactored code
* ➕➖ Replaced `glm` with `Eigen`
* 🔧 Improved compatibility with Ubuntu 20.04

Bug Fixes:
* 🐛 Fixed bugs in convolver related object validity checks  
* 🐛 Added missing dependencies to setup script
* 🐛 Fixed error in absorption and attenuation computations

Doc:
* 📝⚗ Documented design choices for the improved `VaraysEncoder` with prototypes and computation time evaluation
* 💡 Added doxygen descriptions for classes

Continuous Integration:
* ⬆️ using Ubuntu 20.04 instead of 19.10
* ✅ Added tests for Doppler shift related classes
* ✅ Added tests for the new `VaraysEncoder`
* ✅ Added tests for the Python wrapper

vaRays 0.2.0 (2020-04-16)
---------------------------
This is an official release in the 0.2 stable series.

New Features:

* ✨ Added a config file to set options in the vaRays application
* ✨ Added a live convolver based on the [zita-convolver](https://kokkinizita.linuxaudio.org/linuxaudio/index.html) library with SIMD vectorization
* ✨ IRs are generated only when the listener or the sources move
* ✨ Added ray data interpolation functions
* ✨ Added a virtual class for serialization of objects
* ✨ Material properties can be modified with OSC messages
* ✨ Added humidity, temperature and pressure are parameters 

Core:

* 📄 Added GPL 3.0 license
* ⚡️ FFTW3 library used for IR computation
* 📌 Embree version set to v3.5.1
* 📌 ISPC version set to v1.12.0
* ♻️ Separated the ray tracing part and IR writing part into `VaraysContext` and `VaraysEncoder`
* ♻️ Major refactoring of the ray tracing algorithm

Bug Fixes:

* 🐛 Fixed bugs in IR updates 
* 🐛 Fixed numerical errors in ambisonics by changing float to double
* 🐛 Fixed bug in ray direction computation
* 🐛 Fixed IR .wav export function
* 🐛 Fixed bug in reflection computation

Doc:

* 📝 Added documentation for the `RayConvolver` class
* 📝 Added documentation for the `VaraysEncoder::writeIR()` function

Continuous Integration:
* 👷 Added gitlab CI build system
* 🔧 Use pre-built version of ISPC
* ✅ Added tests for the convolver class

vaRays 0.1.0 (2019-01-03)
--------------------------
New features:
- vaRays takes reflections into consideration
- vaRays can take diffraction into consideration
- A cache reduces calculation times can be used
- Exports a .wav file containing the IR in real time
- Example for OSC available
- Multiple parameters can be chosen by the user, such as the amount of rays to shoot, the amount of rays shot from every reflection, the sampling frequency, attenuation with distance, etc.
- Can export ambisonics (SN3D) up to the 6th order
- Can export an obj file to visualize the rebounds seen by the source
- Logger that logs error, warning and debug messages

Known bugs:
- When running the example (tools) built in release with GDB, it will crash.
- Diffraction doesn't work properly.
- Cache doesn't work properly.
