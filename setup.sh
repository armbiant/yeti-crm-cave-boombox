#!/usr/bin/env bash

SOURCE_DIR="$(cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
THIRD_PARTY_DIR="${SOURCE_DIR}/external/third_parties"
EMBREE_SRC_DIR="${THIRD_PARTY_DIR}/embree"
EMBREE_INSTALL_DIR="${SOURCE_DIR}/build/external/embree_install"
SPDLOG_SRC_DIR="${THIRD_PARTY_DIR}/spdlog"
SPDLOG_INSTALL_DIR="${SOURCE_DIR}/build/external/spdlog_install"

set -e

ISPC_VERSION='v1.12.0'
SUDO=''

check_sudo()
{
    if ((${EUID} != 0)); then
        SUDO='sudo'
    fi
}

install_deps()
{
  echo Installing general dependencies
  ${SUDO} apt -y install build-essential cmake wget

  echo Installing VaRays dependencies
  ${SUDO} apt -y install cmake libtbb-dev libglfw3-dev libncurses-dev libeigen3-dev \
                      libsndfile1-dev liblo-dev libz-dev libjsoncpp-dev

  echo Installing LLVM dependencies for compiling Embree
  ${SUDO} apt -y install subversion m4 bison flex python3

  echo Installing dependencies for the convolver
  ${SUDO} apt -y install libfftw3-dev libjack-jackd2-dev jackd2 qjackctl

  echo Installing depdencies for running the simulation
  ${SUDO} apt -y install python3-pip
  # Needed for liblo.
  pip3 install cython
  # Needed for OSC.
  pip3 install pyliblo   
  pip3 install numpy
}

install_ispc()
{
  echo Checking for ispc installation...
  pushd ${THIRD_PARTY_DIR}
  export PATH=$PATH:$(pwd)/ispc-${ISPC_VERSION}-linux/bin

  if ! [ -x "$(command -v ispc)" ]; then
    echo Installing ispc...
    wget https://github.com/ispc/ispc/releases/download/${ISPC_VERSION}/ispc-${ISPC_VERSION}b-linux.tar.gz -O ispc-${ISPC_VERSION}-linux.tar.gz

    tar -xzf ispc-${ISPC_VERSION}-linux.tar.gz
  fi

  popd
}

install_embree()
{
  echo Installing embree...

  git submodule sync --recursive
  git submodule update --init --recursive

  pushd ${EMBREE_SRC_DIR}

  rm -rf build
  mkdir build && cd build
  cmake .. -DCMAKE_INSTALL_PREFIX=${EMBREE_INSTALL_DIR} -DEMBREE_STATIC_LIB=ON
  make -j$(nproc)
  make install

  popd
  echo Finished embree install.
}

install_spdlog()
{
  echo Installing spdlog...

  git submodule sync --recursive
  git submodule update --init --recursive

  pushd ${SPDLOG_SRC_DIR}

  rm -rf build
  mkdir build && cd build
  cmake .. -DCMAKE_INSTALL_PREFIX=${SPDLOG_INSTALL_DIR} -DSPDLOG_ENABLE_PCH=ON
  make -j$(nproc)
  make install

  popd
  echo Finished spdlog install.
}

build_varays()
{
  rm -rf build
  mkdir build
  install_ispc
  install_embree
  install_spdlog

  cd build
  cmake ..
  echo Building...
  make -j$(nproc)

  ${SUDO} make install
  ${SUDO} ldconfig
}

check_sudo
install_deps
build_varays
