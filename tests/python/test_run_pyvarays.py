"""Unit tests for tools/run_pyvarays.py."""

import os
import sys
import threading
import unittest

import liblo
import numpy as np

import pyvarays as pv
import run_pyvarays

DATA_DIR = os.path.realpath(os.path.join(
    os.path.dirname(__file__),
    "../../tools/res/"
))

PARAMS_FILE = os.path.realpath(os.path.join(
    DATA_DIR,
    "Config/vaRaysAppConfig.json"
))


# Start OSC server to check that the pyvarays osc server is running.
osc_server = liblo.Server(3000)
SERVER_IS_RUNNING = False
def is_running(path, argv, types, data):
    global SERVER_IS_RUNNING
    SERVER_IS_RUNNING = argv[0]
osc_server.add_method("/is_running", "i", is_running)

PYVARAYS_PORT = 9000


class TestMain(unittest.TestCase):
    def setUp(self):
        global SERVER_IS_RUNNING
        SERVER_IS_RUNNING = False
        # Start pyvarays osc server.
        self.pyvarays_thread = threading.Thread(
            target=run_pyvarays.main,
            args=([],),
            daemon=True
        )
        self.pyvarays_thread.start()

        # Poll pyvarays server until it is running.
        while not SERVER_IS_RUNNING:
            osc_server.recv()

    def tearDown(self):
        global SERVER_IS_RUNNING
        if self.pyvarays_thread.is_alive():
            liblo.send(PYVARAYS_PORT, "/quit")

            self.pyvarays_thread.join()

    # Test OSC commands.
    def test_quit(self):
        # Send quit command to pyvarays osc server.
        liblo.send(PYVARAYS_PORT, "/quit")

        self.pyvarays_thread.join()

        # Assert server thread has stopped.
        self.assertFalse(self.pyvarays_thread.is_alive())


class TestParams(unittest.TestCase):
    def setUp(self):
        self.arg_parser = run_pyvarays.init_arg_parser()

    def test_varays_auralizer_construction(self):
        # Mock args.
        args = self.arg_parser.parse_args(["-C"])

        params = run_pyvarays.Params(PARAMS_FILE, args)

        # Assert RayConvolver instance was constructed succesfully.
        self.assertIsInstance(params.auralizer, pv.VaraysAuralizer)
        self.assertTrue(params.auralizer.is_allocated())

    def test_varays_auralizer_construction_failure(self):
        """Test if the varays auralizer is not constructed successfully,
        Params.parse_args logs an error and exits.
        """
        args = self.arg_parser.parse_args(["-C"])

        # Set invalid varays auralizer params.
        auralizer_params = pv.VaraysAuralizerParams(num_inputs=0)

        with self.assertLogs(logger="run_pyvarays", level="ERROR") as logging_watcher:
            params = run_pyvarays.Params(
                PARAMS_FILE,
                args,
                auralizer_params
            )

            self.assertIn(
                "ERROR:run_pyvarays:Construction of VaraysAuralizer failed. VaraysAuralizer disabled.",
                logging_watcher.output
            )

            self.assertFalse(params.auralizer.is_allocated())


class TestComputeIR(unittest.TestCase):
    def setUp(self):
        self.params = run_pyvarays.Params(PARAMS_FILE)

    def test_add_source(self):
        self.params.sources_to_add["test source"] = np.zeros(3)

        run_pyvarays.compute_ir(self.params)

        self.assertFalse(self.params.sources_to_add)
        self.assertEqual(len(self.params.context.get_sources()), 1)
        source = self.params.context.get_sources()[0]
        self.assertEqual(source.get_name(), "test source")
        self.assertTrue( (source.get_pos() == np.zeros(3)).all() )

    def test_remove_source(self):
        # Add source.
        self.params.context.add_source(
            np.zeros(3),
            "test source"
        )

        self.params.sources_to_remove.append("test source")

        self.assertTrue(self.params.context.get_sources())

        run_pyvarays.compute_ir(self.params)

        self.assertFalse(self.params.sources_to_remove)
        self.assertFalse(self.params.context.get_sources())

    def test_remove_all_sources(self):
        # Add sources.
        for i in range(3):
            self.params.context.add_source(
                np.array([i, 0.0, 0.0]),
                str(i)
            )

        self.params.remove_all_sources = True

        run_pyvarays.compute_ir(self.params)

        self.assertFalse(self.params.remove_all_sources)
        self.assertFalse(self.params.context.get_sources())
        self.assertFalse(self.params.sources_to_remove)
