#!/bin/bash

# Run all python unit tests in tests/python/

SOURCE_DIR="$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)"

if [ "$(jack_wait -c)" == "running" ]; then
  JACKSERV=0
else
  jackd --no-realtime -d dummy -r 48000 &
  JACKSERV=$!
fi

jack_wait -w
python3 -m unittest discover -s $SOURCE_DIR
RET=$?

if [ $JACKSERV -ne 0 ]; then
  kill $JACKSERV
fi

exit $RET
