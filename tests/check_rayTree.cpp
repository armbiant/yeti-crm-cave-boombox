#undef NDEBUG // get assert in release mode

#include "../varays/Context/RayTracer/RootNode.hpp"

using namespace varays;

TreeNode newNodeWithLength(float length)
{
    return TreeNode{
        .coordinates = Eigen::Vector3f(), 
        .rayDirection = Eigen::Vector3f(), 
        .normal = Eigen::Vector3f(), 
        .length = length, 
        .firstRayDirection = Eigen::Vector3f(), 
        .outgoingEnergies = {}, 
        .audioMat = {}, 
        .reflectionTypes = {}};
}

float randomLength()
{
    return 100.0f * rand() / static_cast<float>(RAND_MAX);
}

int main()
{
    RootNode rayTree;

    // Create vector that contains the rayTree lengths to compare later
    std::vector<std::vector<float>> lengths;
    for (int i = 0; i < 4; ++i)
    {
        lengths.emplace_back(std::vector<float>());
    }

    // Fill the rayTree with nodes that have random distances
    for (int i = 0; i < 10; ++i)
    {
        float randLength0 = randomLength();
        TreeNode newNode0 = newNodeWithLength(randLength0);
        if (rand() % 2)
        {
            for (int j = 0; j < 10; ++j)
            {   
                float randLength1 = randomLength();
                TreeNode newNode1 = newNodeWithLength(randLength1);
                if (rand() % 2)
                {
                    for (int k = 0; k < 10; ++k)
                    {
                        float randLength2 = randomLength();
                        TreeNode newNode2 = newNodeWithLength(randLength2);
                        if (rand() % 2)
                        {
                            for (int l = 0; l < 10; ++l)
                            {
                                float randLength3 = randomLength();
                                TreeNode newNode3 = newNodeWithLength(randLength3);
                                newNode2.children.emplace_back(newNode3);
                                lengths[3].emplace_back(randLength3);
                            }
                        }
                        else
                            lengths[2].emplace_back(randLength2);
                        newNode1.children.emplace_back(newNode2);
                    }
                }
                else
                    lengths[1].emplace_back(randLength1);
                newNode0.children.emplace_back(newNode1);
            }
        }
        else
            lengths[0].emplace_back(randLength0);
        rayTree.children.emplace_back(newNode0);
    }
    // Compare findEndNodes results and the lengths we saved to see if the results are equivalent and in the same order
    for (int i = 0; i < 4; ++i)
    {
        std::vector<TreeNode*> endNodes = rayTree.findEndNodes(i + 1);
        assert(lengths[i].size() == endNodes.size());
        for (size_t j = 0; j < lengths[i].size(); ++j)
        {
            assert(lengths[i][j] == endNodes[j]->length);
        }
    }
    return 0;
}