#!/bin/bash

# Start a jack server if required
if [ "$(jack_wait -c)" == "running" ]; then
  JACKSERV=0
else
  jackd --no-realtime -d dummy -r 48000 &
  JACKSERV=$!
fi
jack_wait -w

# Run the compiled cpp test binary
./check_jackPortOperations
ret=$?

# Kill jack server if it was created in this script
if [  $JACKSERV -ne 0 ]; then
  kill $JACKSERV
fi

exit $ret