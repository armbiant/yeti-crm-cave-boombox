#undef NDEBUG // get assert in release mode

#include "../varays/Context/RayTracer/utils.hpp"
#include "./testUtils.hpp"

using namespace varays;

int main()
{
    for (int i = 0; i < 1000; ++i)
    {
        std::vector<float> frequencies;
        // initialize random seed
        srand (time(NULL));

        for (int j = 0; j < 4; ++j)
            frequencies.push_back(testUtils::unitRandFloat() * 9980 + 20);

        materialProperty absorption{{frequencies[0], testUtils::unitRandFloat() / 2}, {frequencies[1], testUtils::unitRandFloat() / 2.f}, {frequencies[2], testUtils::unitRandFloat() / 2}, {frequencies[3], testUtils::unitRandFloat() / 2}};
        materialProperty transmission{{frequencies[0], testUtils::unitRandFloat() / 2}, {frequencies[1], testUtils::unitRandFloat() / 2.f}, {frequencies[2], testUtils::unitRandFloat() / 2}, {frequencies[3], testUtils::unitRandFloat() / 2}};
        materialProperty scattering{{frequencies[0], testUtils::unitRandFloat()}, {frequencies[1], testUtils::unitRandFloat()}, {frequencies[2], testUtils::unitRandFloat()}, {frequencies[3], testUtils::unitRandFloat() / 2}};
        AudioMat material{absorption, transmission, scattering};

        auto type = utils::NodeType(rand() % 3);
        if (type != utils::NodeType::INVALID)
        {
            std::map<int, varays::energy> incomingEnergies;
            int sources = rand() % 20 + 1;
            for (int j = 0; j < sources; ++j)
            {
                varays::energy energyMap;
                for (const auto freq : frequencies)
                    energyMap[freq] = testUtils::unitRandFloat();
                incomingEnergies[j] = energyMap;
            }
            utils::NodeInfo nodeInfo{type, Eigen::Vector3f(), Eigen::Vector3f(), testUtils::unitRandFloat() * 100.f, material};
            TreeNode node = utils::makeNode(nodeInfo, Eigen::Vector3f(), incomingEnergies, testUtils::unitRandFloat() * 100.f, std::vector<SoundEvent::Reflection>());
            std::map<int, varays::Energies> outgoingEnergies = node.outgoingEnergies;
            for (const auto& outgoingEnergy : outgoingEnergies)
            {
                for(const auto& specularEnergy : outgoingEnergy.second.specular)
                    assert(specularEnergy.second > 0.0 && specularEnergy.second < 1.0);
                for (const auto& diffuseEnergy : outgoingEnergy.second.diffuse)
                    assert(diffuseEnergy.second > 0.0 && diffuseEnergy.second < 1.0);
            }
        }
    }
    return 0;
}