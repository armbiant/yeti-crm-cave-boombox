#undef NDEBUG // get assert in release mode

#include "../varays/Context/RayTracer/utils.hpp"
#include "./testUtils.hpp"

using namespace varays;

int main()
{
    for (int i = 0; i < 1000; ++i)
    {
        // create the TreeNode
        Eigen::Vector3f normalizedDir = (-testUtils::randVec3()).normalized();
        TreeNode newNode = {
            .coordinates = Eigen::Vector3f(0.0, 0.0, 0.0),
            .rayDirection = normalizedDir,
            .normal = testUtils::randVec3(),
            .length = testUtils::unitRandFloat(),
            .firstRayDirection = utils::reverseSphericalVector(Eigen::Vector3f(1.f, atan2(normalizedDir[1], normalizedDir[0]), acos(normalizedDir[2]))),
            .outgoingEnergies = std::map<int, varays::Energies>(),
            .audioMat = AudioMat(),
            .reflectionTypes = std::vector<SoundEvent::Reflection>()
            };

        // create the sources
        int sourcesAmount = rand() % 10 + 1;
        std::vector<AudioSource> sources;
        std::vector<bool> visibilities;
        uint32_t counter = 0;
        for (int j = 0; j < sourcesAmount; ++j)
        {
            sources.emplace_back(AudioSource(j, "source", testUtils::randVec3(), {100, 200, 300}, 1.0));
            bool visible = rand() % 2;
            if (visible)
                ++counter;
            visibilities.emplace_back(visible);
        }
        utils::computeSoundEvents(visibilities, sources, newNode);
        assert(newNode.soundEvents.size() == counter);
    }
    return 0;
}