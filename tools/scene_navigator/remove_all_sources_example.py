import time

from osc_sender import OscSender

sender = OscSender(9000)

sender.send_user_position(0.0, 0.0, 0.0)
time.sleep(0.15)

for i in range(10):
    sender.create_new_source(
        str(i),
        float(round(i*0.4 - 2, 1)),
        0.5,
        0.5
    )
    time.sleep(0.5)

sender.remove_all_sources()
