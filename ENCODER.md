# VaraysEncoder - Writing the IR from the ray data

A new IR is written for each source every time a new generation of rays has been traced. The number of channels of that IR is determined by the ambisonic order chosen by the user ((order+1)² channels). Here is an explanation of how the IR is written from the rays data.

### Data we have (and need):
#### Ray data for every ray that reaches a source:
- Band-wise energy content of the ray
- Total length of the ray
- Direction of the ray (used for ambisonics)

#### Context data:
- Bitrate of the IR
- Speed of sound for the current conditions
- Ambisonic order of the IR

### Generating Ambisonic Room Impulse Responses in Time-domain - `VaraysEncoder::writeIR()`

Each ray that reaches a listener from a source is a Dirac impulse which is filtered by the surfaces participating in its propagation due to their acoustic properties (absorption, transmission and scattering) and is attenuated due to air absorption given the atmospheric conditions. Since the acoustic properties of materials are defined for 8 octave bands between the range of 125 - 16000 Hz and the absorption coefficients of air in these bands are pre computed, the energy content in each of these bands for the rays reaching the listener from any given source is available.

To convert this known energy content and the path length of the rays to a mono impulse response, we first generate a train of delayed (propagation length) and weighted (energy) impulses for each octave band. These impulse trains are then filtered by their corresponding perfect reconstruction octave band filters and are summed to give the room impulse response. For an ambisonic impulse response of a given order, the same structure is repeated for each ambisonic channel with the impulses being scaled additionally by the ambisonic coefficient due to its direction of arrival at the listener which is computed by the `HOA::getAmbi()` function. Currently, higher ambisonic impulse responses of up to order 6 can be generated.

The perfect reconstruction filter bank used in vaRays is an infinite impulse response (IIR) Linkwitz-Riley filter tree, which is described in detail [here](./Playground/IR_Generation/Python/IIR_approach/LinkwitzRileyFilterTree.ipynb). The LR filter tree is computed on startup for the frequency bands defined in the materials.json file, which contains the acoustic properties of materials, and the given sample rate. When the [real-time convolver feature](./CONVOLVER.md) is used, the sample rate is obtained from the Jack process. The number of filter bands in the simulation must strictly be a power of 2.

The scripts and test programs that lead to the choice of this impulse response computation technique can be found [here](./Playground/IR_Generation).