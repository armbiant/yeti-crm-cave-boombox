# The vaRays Auralizer

## Dependencies
- Jack2
- fftw3f

(see [CMakeLists.txt](./CMakeLists.txt).)

## The `VaraysAuralizer` Class
The vaRays auralizer has two main parts; 1) a convolution engine and 2) a Doppler shift engine. The convolution engine is based on *JConvolver*, an application interface for the *zita-convolver* library (both by [Fons Adriaensen (c)](https://kokkinizita.linuxaudio.org/linuxaudio/index.html)) and the Doppler shift engine uses a fractional delay line approach to simulate Doppler shift in dynamic scenes. The `VaraysAuralizer` class acts as the interface for controlling the two parts. It was previously named `RayConvolver`, which inherited from the `JConvolverWrapper` class (ported from JConvolver - see [vaRays 0.2.0](https://gitlab.com/sat-metalab/vaRays/-/releases#0.2.0)).

On instantiating a `VaraysAuralizer` object, a set of jack output ports are created for ambisonic output stream numbered according to the [ACN](https://en.wikipedia.org/wiki/Ambisonic_data_exchange_formats#ACN) (ambisonic channel numbering) convention. The partition sizes in the convolver are determined by the maximum number of inputs, number of outputs and the maximum size of the impulse response specified during the instantiation of the `VaraysAuralizer` object. 

The `VaraysAuralizer` class has three functions: 

- `newSource(const std::string& sourceName)`: creates a new jack input named `sourceName` and connects it to the ambisonic output channels via the convolver.
- `deleteSource(const std::string& sourceName)`: deletes the specified jack input and destroys the convolution resources for the input.
- `updateIR(const std::string& ip1, uint32_t nchan, const std::vector<float>& data)`: updates the IR for the given input channel with the new IR given in `data`.
- `updateDelayLines(const std::string& sourceName, const SoundEvent& directSoundEvent, bool sourceInsight = true, bool teleport = false)`: updates the delay line with smooth linear delay update to simulate Doppler shift.

`vaRays` (see [./tools/main.cpp](./tools/main.cpp)) manages the acoustic scene along with the auralization using these functions through commands sent via `OSC`. [This python script](./tools/scene_navigator/trajectory_example.py) shows how `OSC` messages can be used to control the acoustic scene. 

## Using the vaRays Auralizer
1. If you choose to use the vaRays auralizer, you need to have jack2 installed. If you already have it installed, skip to step 2. If not, first install jack2 by running:
```sh
sudo apt-get install libjack-jackd2-dev jackd2 
sudo apt-get install qjackctl # GUI for routing jack channels
```
`qjackctl` is a GUI for jack that lets you configure jack settings and handle jack connections.

2. Open qjackctl and click "Start" to start a jack server. 

3. Start vaRays with `-C` (uppercase C) flag to enable the auralizer since it is not active by default. For example,
    ```sh
    ./build/tools/vaRays -o ./tools/res/cube.obj -C
    ```
    instantiates `vaRays` in the environment stored in `cube.obj`, with an ambisonic convolver. The number of ambisonic channels is determined by the value of `AmbisonicOrder` set in the [vaRaysAppConfig.json](tools/res/Config/vaRaysAppConfig.json) file. Simulating Doppler shift is optional and can be enabled by setting `EnableDopplerShift` to `true`. The file also contains settings for Doppler shift, such as the Lagrange interpolation order and maximum allowed relative velocity between the sources and the listener.

    **NOTE:** When the auralizer feature is not activated, the Doppler shift feature is automatically disabled. However, when the auralizer is used along with Doppler shift, the impulse responses saved into .wav files will not contain the direct sound. 

4. To create new sources, send a `new_source` OSC message to vaRays with the name of the source you want to create (see [this python script](./tools/scene_navigator/trajectory_example.py)). The jack input port created for the will be of the same name. 

5. To see the active ports, click on "Connect" in qjackctl. This will open a window with the active jack input and output ports listed. In the output pane, you should see a jack client named "vaRays Convolver", under which you will see all the ambisonic output channels listed as "out-0", "out-1", etc. In the input pane, you should see the input port created for the new source under the "vaRays Convolver" client. 

6. You can now route an audio stream to the vaRays source input channel by dragging the audio stream from the output pane to the input stream of the source. 

For testing the auralizer with the an audio stream from a browser window, do the following: (Make sure that a jack server is active while running these commands)
```sh
sudo apt-get install pulseaudio-module-jack
pactl load-module module-jack-sink channels=2
pactl load-module module-jack-source channels=2
pacmd set-default-sink jack_out
sudo apt-get install pavucontrol
```
Now run pavucontrol. Under the "Playback" tab, select the option `Jack sink (PulseAudio JACK Sink)` for the browser. (Note: the browser must be playing some audio for it to be listed in the "Playback" tab.)

Now, in the "Connect" window of qjackctl, you can drag "PulseAudio JACK Sink" from the Output pane to your vaRays source channel. To listen to convolved output, connect a channel from "vaRays Convolver" in the output pane to a channel under "system" in the input pane. You should hear the convolved audio! You must use an ambisonic decoder (for example, [SATIE](https://gitlab.com/sat-metalab/SATIE)) to hear the full ambisonic output.

# About the Convolver
The convolver uses a non-uniform partition overlap-and-save (NUPOLS) approach to convolve input audio streams with ambisonic room-impulse-responses generated by vaRays. A good description of this method can be found in [Chapter 6](http://publications.rwth-aachen.de/record/466561/files/466561.pdf#page=163) of Frank Wefers' doctoral thesis.

In short, the goal of NUPOLS is to minimize computational cost of the convolution process while minimizing output latency by partitioning long impulse responses (IRs) into chunks of non-decreasing lengths, essentially breaking long convolutions down to shorter ones. Generally, convolving two signals in the frequency domain is cheaper for longer IRs (2<sup>n</sup>) but induces a large latency in the output. The smaller the IR is, the lower is the output latency. In the NUPOLS technique, the IRs are partitioned into shorter segments towards the beginning of the impulse response to produce low latency and are partitions into longer segments towards the rear-end to reduce the computational cost.

### Updating the Impulse Response
Since the zita-convolver library does not provide the option to dynamically update impulse responses in real-time, we had to make several significant changes, considering different strategies for replacing old impulse response partitions with new ones. 

One of the main issues with replacing IR partitions is that, successive IRs generated by the ray tracer may be different by a non-determistic amount even if there is no significant change in source-listener-scene configuration between the two iterations, i.e. some paths may be traced in some iterations and may not be traced in other iterations, hence causing significant energy fluctuations in the IRs. This is mainly due to the stochastic nature of the ray tracer with no mechanism to ensure re-tracing of identified significant paths in place, yet. Hence, there may be perceivable and sudden volume differences during IR update that almost sound like clicks.

In order to avoid these clicks, the transition between two IRs must be smooth. One way to accomplish this is to cross-fade the outputs of two simultaneous convolvers, one with the old IR and the other with the new one. Since convolution is a linear operation, cross-fading the outputs of the two convolvers may be realized as fading-in the input to the new convolver and simultaneously fading-out the input to the old convolver. We use a half-hann-window to accomplish this in the time domain. During the transition period for a given IR partition, the output of the convolution is the sum of convolutions of faded-out input signal with the old IR and the faded-in input signal with the new IR. 

The other issue is with the timing of partition updates. Some of the filter partition update/exchange strategies are described [here](http://publications.rwth-aachen.de/record/466561/files/466561.pdf#page=205). The two main strategies considered were the immediate coherent exchange ([fig. 6.18 (a)](http://publications.rwth-aachen.de/record/466561/files/466561.pdf#page=206)), where all the IR partitions are replaced simultaneously as soon as an IR update trigger is received, and the successive asynchronous exchange ([fig. 6.18 (c)](http://publications.rwth-aachen.de/record/466561/files/466561.pdf#page=206)), where the IR partitions are replaced successively as the buffered input samples from the audio stream are convolved. 

For a completely smooth transition, no input samples buffered before the update trigger must be convolved with the new IR and no samples buffered after the trigger should be convolved with the old IR [^1]. The latter of the two above approaches ensures this. However, the former approach is used in the vaRays convolver since there was an amplitude modulation-like effect with the successive asynchronous approach in the brief tests conducted. A known consequence of using the immediate coherent exchange strategy is that when the listener and source move closer from a very far distance in a very short span of time, the older buffered samples are convolved with the new IR that has significantly higher energy, causing a slapback delay-like effect, which is undesirable in the context of room acoustic modeling. Further testing and improvements could make the successive asynchronous exchange strategy usable in the vaRays convolver.

[^1]: Brandtsegg, Ø., Saue, S., & Lazzarini, V. (2018). Live convolution with time-varying filters. Applied Sciences, 8(1), 103.

# The Doppler Engine

[Doppler shift](https://en.wikipedia.org/wiki/Doppler_effect) is a physical phenomenon in which the pitch of the sound emitted by an object is perceived to change due to its relative velocity with respect to the listener. A classic example is of a race car whose pitch increases as it approaches and reduces as it whizzes past and moves away from the listener. In vaRays, this effect is implemented in the [DopplerEngine](varays/Convolver/DopplerEngine.hpp) class which makes use of delay lines with variable delays producing the appropriate frequency shifts in a dynamic scene.

The delay lines use [Lagrange interpolation](https://ccrma.stanford.edu/~jos/pasp/Lagrange_Interpolation.html) to interpolate between two sampling instances when the delay is a fractional multiple of the sampling period. It is essential a filter whose output depends on samples in the delay line around the given delay value. The filter coefficients are computed as given [here](https://ccrma.stanford.edu/~jos/pasp/Explicit_Lagrange_Coefficient_Formulas.html). The choice of filter order would depend on required accuracy of results and computational time. Higher the filter order, greater the filter accuracy and greater the computational time (an analysis of results for different filter orders is given [here](https://ccrma.stanford.edu/~jos/pasp/Lagrange_Frequency_Response_Examples.html)).

Emulation of Doppler shift requires that the instantaneous velocities of the listener and the sources are known. Although physics engines in some popular game engines may be able to provide the instantaneous velocities, it is implemented here by recording the times at which delay line updates are triggered. When the `DopplerEngine::updateDelay()` function is called, the computer clock time is recorded and the number of sampling instances elapsed from the previous update is computed. The delay update is then performed linearly in steps over the computed number of samples to produce the Doppler effect. Currently, if the update time difference is greater than 0.5s, the delay is instantaneously updated to the new value assuming the object teleported from its previous location.
