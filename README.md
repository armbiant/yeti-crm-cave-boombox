# vaRays

[vaRays](https://gitlab.com/sat-metalab/vaRays) is a library for computing realtime Impulse Response for virtual acoustic of 3D environment.

vaRays loads and initial 3D model in which one or many sources can be created, moved and deleted, along with a controllable listener position. Controls are currently done by external software through a basic OSC protocol. vaRays then outputs in real time an impulse response file for each audio source, allowing the calculation of a convolution reverberation by an external software.

See instruction for [contributing code](./CONTRIBUTING.md).

Here is an [explanation](./ENCODER.md) of how the impulse responses are written from the data gathered by the rays.

Currently, only Ubuntu is a supported OS for vaRays.

## Table of Contents

[[_TOC_]]

## Overview

We presented the vaRays project at the [SMC 2021 conference](https://smc2021conference.org/program/).

The [video presentation](https://www.youtube.com/watch?v=8rurmVUCddA) and the paper are available under the name *Live Ray Tracing and Auralization of 3D Audio Scenes with vaRays* by Émile Ouellet-Delorme, Harish Venkatesan, Emmanuel Durand and Nicolas Bouillot.

## Installation

Start by creating a copy of the repository on your local computer:

```bash
git clone git@gitlab.com:sat-metalab/vaRays.git
cd vaRays
```

Run the installation script:
*Note*: do not run the script as sudo since it is called within the script.

```bash
./setup.sh
```

You are now ready to test your vaRays installation [by launching the Python test](#launching-the-python-test).

### Python wrapper

If Python environment files are found, a Python wrapper is built alongside the vaRays library. To install it, just (after having built the library):

```bash
sudo make install
```

Then try loading the resulting Python module:
```python
import pyvarays
```

## Using vaRays

### Exporting a scene from Blender:

A few things need to be done before and while exporting a scene from Blender in order to be compatible with vaRays.
- Triangulate object: In edit mode, press "A" to select every object face, then press "CTRL+T" to triangulate them all.
- Objects and materials: Objects in the scene can be assigned materials from those listed in the [materials.json](./tools/res/Config/materials.json) file (case-sensitive). If an object has a material that does not exist in the database or has no material, default material properties will be used.
- Exporting: export the scene as a .obj.

### Materials in vaRays
**Naming Convention** - all lowercase letters with words separated by an underscore "_". Eg. "rough_concrete", "acoustical_plaster", "studio_curtain".

The absorption coefficients of the materials in the database are from the [appendix](https://cds.cern.ch/record/1251519/files/978-3-540-48830-9_BookBackMatter.pdf) of the book *Auralization*[^1] by M. Vorlander and J. Summers. In order to add new materials or edit properties of existing materials, make changes in the [materials.json](./tools/res/Config/materials.json) file.

[^1]: Vorlaender, Michael & Summers, Jason. (2008). Auralization: Fundamentals of Acoustics, Modelling, Simulation, Algorithms, and Acoustic Virtual Reality. The Journal of the Acoustical Society of America. 123. 4028. 10.1121/1.2908264.

### Using and testing example

#### Launching vaRays

    # From the root folder
    cd build/tools
    ./vaRays

Use -h to visualize the vaRays launch options.

#### Launching the Python test

Still in the vaRays folder, run the following commmands:

    # From the root folder
    cd tools/scene_navigator
    python3 trajectory_example.py

You should get an output similar to the following:

```
$ python3 trajectory_example.py
User position changed to 0.0 0.0 0.0
 
created new source sound at 2.0 0.5 0.5
 
Moved source sound at -2.0 0.5 0.5
 
Moved source sound at -1.6 0.5 0.5
 
Moved source sound at -1.2 0.5 0.5
 
Moved source sound at -0.8 0.5 0.5
 
Moved source sound at -0.4 0.5 0.5
 
Moved source sound at 0.0 0.5 0.5
 
Moved source sound at 0.4 0.5 0.5
 
Moved source sound at 0.8 0.5 0.5
 
Moved source sound at 1.2 0.5 0.5
 
Moved source sound at 1.6 0.5 0.5
 
Moved source sound at 2.0 0.5 0.5
 
Removed source sound
 
Done!

```

### Using example with OSC
##### Input:

	/new_source sfff [source unique ID, x, y, z]
	/remove_source s [source unique ID]
	/move_source sfff [source unique ID, x, y, z]
    /editor_position fff [x, y, z]

##### Output:

    /ir_file s ["./name of the IR file)"]
    
##### Using with other software:

We used EiS as a software to control the position sources and the listener. See [here](https://vimeo.com/306202441)

License
-------

vaRays is released under the terms of the GNU GPL v3 or above.

Authors
-------
See [here](AUTHORS.md).

Credits
--------
Credit to Robert Smith for his ObjLoader file.
Original version can be found here : https://github.com/Bly7/OBJ-Loader

Sponsors
--------
vaRays was created at the Society for Arts and Technology (SAT) to give to artists a powerful tool virtual acoustic in immersive environments.

This project is made possible thanks to the Society for Arts and Technologies. [SAT](http://www.sat.qc.ca/) and to the Ministère de l'Économie, de la Science et de l'Innovation du Québec (MESI).

