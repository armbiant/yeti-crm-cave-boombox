// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef __DOPPLERENGINE__
#define __DOPPLERENGINE__

#include <chrono>
#include <map>
#include <memory>
#include <vector>

#include <Eigen/Dense>

#include "../../Common/SoundEvent.hpp"
#include "../../Utils/Logger.hpp"
#include "./DelayLine.hpp"

using ChronoResolution = std::chrono::milliseconds;
using ChronoTimePoint = std::chrono::system_clock::time_point;

namespace varays
{

class DopplerEngine
{
  public:
    /*
     * Constructor
     *
     * @param   numOutChannels    Number of output channels corresponding to the ambisonic order
     * @param   interpFilterOrder Order of the Lagrange interpolation filter for fraction interpolation
     * @param   velocityLimit     Maximum relative velocity between the listener and sources beyond which movement is considered teleportation (m/s)
     * @param   speedOfSound      Speed of sound in air (m/s)
     */
    DopplerEngine(unsigned int numOutChannels, unsigned int interpFilterOrder = 4, float velocityLimit = 50.0f, float speedOfSound = 343.0f);

    /*
     * Set audio device sample rate and buffer size
     *
     * @param	sampleRate	Sample rate of the Jack server in Hz
     * @param	bufferSize	Jack buffer size
     */
    void setAudioDeviceProperties(unsigned int sampleRate, unsigned int bufferSize);

    /*
     * Create input for a new source in the scene
     *
     * @param   inputName   Name of the source. Must match the name of the corresponding jack input.
     */
    void inputCreate(const std::string& inputName);

    /*
     * Delete input of a source
     *
     * Called when a source is removed from the scene.
     *
     * @param   inputName   Name of the source to be deleted. Must match the name of the corresponding jack input.
     */
    void inputDelete(const std::string& inputName);

    /*
     * Update the properties of a given source
     *
     * @param   inputName   Name of the source to be updated
     * @param   sndEvent    SoundEvent of the direct sound from the source to the listener
     * @param   insight     True if visible, false if occluded.
     * @param	  teleport    Delay is immediately set if true, linearly updated if false.
     */
    void updateSource(const std::string& inputName, const SoundEvent& sndEvent, bool insight = true, bool teleport = false);

    /*
     * Load a block of samples into a source's delay line
     *
     * @param   inputName   Name of the source whole delay line is to be loaded with new samples
     * @param   inBlock     Vector containing a mono stream of samples
     */
    void loadInputBlock(const std::string& inputName, const std::vector<float>& inBlock);

    /*
     * Load a block of samples into a source's delay line
     *
     * @param   inputName   Name of the source whole delay line is to be loaded with new samples
     * @param   inBlock     Float array containing a mono stream of samples
     * @param   numSamples  Number of samples in the stream
     */
    void loadInputBlock(const std::string& inputName, const float* inBlock, int numSamples);

    /*
     * Get a delayed block of samples from a source
     *
     * @param   channel     Channel number
     * @return              Vector containing mono stream of samples from the given channel number
     */
    std::vector<float> getOutputBlock(int channel) { return dopplerOut_[channel]; }

    /*
     * Computes delayed samples for the given number of loaded samples in the delay lines
     */
    void process();

    /*
     * Check if audio device properties have been set. Print warning if false.
     */
    bool isConfigured() const
    {
        if (configured_)
            return true;
        logger_.log(Logger::WARNING, "Audio device properties not set!");
        return false;
    }

  private:
    struct SourceData
    {
        std::unique_ptr<DelayLine> dLine_{};
        std::vector<double> ambiCoeff_;
        bool insight_{false};
        ChronoTimePoint lastUpdateTime_;

        SourceData(float maxDelayGrowthRate, unsigned int lagrangeFilterOrder)
        {
            dLine_ = std::make_unique<DelayLine>(maxDelayGrowthRate, lagrangeFilterOrder);
            lastUpdateTime_ = std::chrono::high_resolution_clock::now();
        }
    };
    std::map<std::string, SourceData> sourceMap_; /** Map of delay lines maintained for the sources in the scene*/
    unsigned int numOutChannels_{0}; /** Number of output channels corresponding to the ambisonic order*/
    unsigned int jackFragSize_{0}; /** Jack fragment size*/
    unsigned int sampleRate_{0}; /** Sample rate in Hz*/
    unsigned int interpolationOrder_{0}; /** Order of lagrange fractional interpolation filter*/
    float speedOfSoundInv_{1.0f / 343.0f}; /** 1 / speed of sound*/
    float maxDelayGrowthRate_{50.0f / 343.0f}; /** Maximum rate of change of delay*/

    std::vector<std::vector<float>> dopplerOut_{}; /** Vector of mono delayed streams corresponding to the ambisonic channels*/
    Logger logger_{"DopplerEngine"};

    bool configured_{false};
};

}; // namespace varays
#endif //__DOPPLERENGINE__