// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <algorithm>

#include "./DopplerEngine.hpp"
#include "../../Encoder/Ambisonics.hpp"

namespace varays
{

DopplerEngine::DopplerEngine(unsigned int numOutChannels, unsigned int interpFilterOrder, float velocityLimit, float speedOfSound)
    : numOutChannels_(numOutChannels)
    , interpolationOrder_(interpFilterOrder)
    , speedOfSoundInv_(1.0f / speedOfSound)
    , maxDelayGrowthRate_(velocityLimit * speedOfSoundInv_)
{
}

void DopplerEngine::setAudioDeviceProperties(unsigned int sampleRate, unsigned int bufferSize)
{
    sampleRate_ = sampleRate;
    jackFragSize_ = bufferSize;

    dopplerOut_.resize(numOutChannels_, std::vector<float>(jackFragSize_, 0.0f));
    configured_ = true;
}

void DopplerEngine::inputCreate(const std::string& inputName)
{
    if (!isConfigured())
        return;

    if (sourceMap_.find(inputName) != sourceMap_.end())
    {
        logger_.log(Logger::WARNING, "Input named \"" + inputName + "\" already exists.");
        return;
    }
    sourceMap_.insert({inputName, SourceData(maxDelayGrowthRate_, interpolationOrder_)});
}

void DopplerEngine::inputDelete(const std::string& inputName)
{
    auto it = sourceMap_.find(inputName);
    if (it == sourceMap_.end())
    {
        logger_.log(Logger::WARNING, "Doppler input named \"" + inputName + "\" does not exist.");
        return;
    }
    sourceMap_.erase(it);
}

void DopplerEngine::updateSource(const std::string& inputName, const SoundEvent& sndEvent, bool insight, bool teleport)
{
    auto it = sourceMap_.find(inputName);
    if (it == sourceMap_.end())
    {
        logger_.log(Logger::WARNING, "Doppler input named \"" + inputName + "\" does not exist.");
        return;
    }

    auto updateTime = std::chrono::high_resolution_clock::now();
    auto elapsed = std::chrono::duration_cast<ChronoResolution>(updateTime - it->second.lastUpdateTime_);
    it->second.lastUpdateTime_ = updateTime;

    if (insight)
    {
        if (!it->second.insight_)
        {
            teleport = true;
            it->second.insight_ = true;
        }
    }
    else
    {
        it->second.insight_ = false;
        return;
    }

    unsigned int numSamples = static_cast<float>(elapsed.count()) * (0.001f * sampleRate_);
    it->second.ambiCoeff_ = HOA::getAmbi(sndEvent.sphericalCoordinates_[1], sndEvent.sphericalCoordinates_[2], std::sqrt(numOutChannels_) - 1);
    float gain = std::sqrt(sndEvent.getEnergies()[0]);
    std::transform(it->second.ambiCoeff_.begin(), it->second.ambiCoeff_.end(), it->second.ambiCoeff_.begin(), std::bind1st(std::multiplies<float>(), gain));
    it->second.dLine_->setDelay(sndEvent.length_ * speedOfSoundInv_ * sampleRate_, (teleport || (numSamples > sampleRate_ / 2)) ? 0 : numSamples);
}

void DopplerEngine::loadInputBlock(const std::string& inputName, const float* inBlock, int numSamples)
{
    auto it = sourceMap_.find(inputName);
    if (it == sourceMap_.end())
    {
        logger_.log(Logger::WARNING, "Doppler input named \"" + inputName + "\" does not exist.");
        return;
    }

    if (it->second.insight_)
        it->second.dLine_->writeSamples(inBlock, numSamples);
    return;
}

void DopplerEngine::loadInputBlock(const std::string& inputName, const std::vector<float>& inBlock)
{
    auto it = sourceMap_.find(inputName);
    if (it == sourceMap_.end())
    {
        logger_.log(Logger::WARNING, "Doppler input named \"" + inputName + "\" does not exist.");
        return;
    }

    if (it->second.insight_)
        it->second.dLine_->writeSamples(inBlock);
    return;
}

void DopplerEngine::process()
{
    if (!isConfigured())
        return;

    // clear previous output
    std::for_each(dopplerOut_.begin(), dopplerOut_.end(), [](std::vector<float>& ch) { std::fill(ch.begin(), ch.end(), 0.0f); });
    for (auto& src : sourceMap_)
    {
        if (!src.second.insight_)
            continue;
        std::vector<float> dLineOutput(jackFragSize_);
        src.second.dLine_->readSamples(dLineOutput);
        for (size_t ch = 0; ch < dopplerOut_.size(); ++ch)
        {
            for (size_t samp = 0; samp < jackFragSize_; ++samp)
                dopplerOut_[ch][samp] += (src.second.ambiCoeff_[ch] * dLineOutput[samp]);
        }
    }
}

}; // namespace varays