//  Copyright (C) 2019- [SAT] Metalab (Harish Venkatesan)
//  Copyright (C) 2006-2011 Fons Adriaensen <fons@linuxaudio.org>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "./VaraysAuralizer.hpp"

namespace varays
{

VaraysAuralizer::VaraysAuralizer(VaraysAuralizerParams params)
    : jackserv_(params.jackServerName_)
    , ninp_(params.numInputs_)
    , nout_(params.numOutputs_)
{
    ERROR_CODE stat = NOERR;

    if (params.enableDoppler_)
        dopplerEngine_ = std::make_unique<DopplerEngine>(nout_, params.dopplerInterpOrder_, params.dopplerMaxVelocity_, params.dopplerSpeedOfSound_);

    jclient_ = std::make_unique<Jclient>(jackname_.c_str(), jackserv_.c_str(), &convproc_, dopplerEngine_.get());
    if ((!jclient_) || (!jclient_->isReady()))
    {
        logger_.log(Logger::CRITICAL, "Jack client initialization failed");
        return;
    }

    size_ = params.convSize_;
    auto partSize = params.convPartitionSize_;
    auto dens = params.convDensity_;

    fsamp_ = jclient_->fsamp();
    fragm_ = jclient_->fragm();

    if (dopplerEnabled())
        dopplerEngine_->setAudioDeviceProperties(fsamp_, fragm_);

    if ((ninp_ == 0) || (ninp_ > Convproc::MAXINP))
    {
        logger_.log(Logger::ERROR, "Number of inputs (" + std::to_string(ninp_) + ") is out of range.");
        stat = ERR_IONUM;
    }
    if ((nout_ == 0) || (nout_ > Convproc::MAXOUT))
    {
        logger_.log(Logger::ERROR, "Number of outputs (" + std::to_string(nout_) + ") is out of range.");
        stat = ERR_IONUM;
    }
    if ((partSize & (partSize - 1)) || (partSize < Convproc::MINPART) || (partSize > Convproc::MAXPART))
    {
        logger_.log(Logger::ERROR, "Partition size (" + std::to_string(partSize) + ") is illegal.");
        stat = ERR_PARAM;
    }
    if (partSize > Convproc::MAXDIVIS * fragm_)
    {
        partSize = Convproc::MAXDIVIS * fragm_;
        logger_.log(Logger::WARNING, "Partition size adjusted to " + std::to_string(partSize));
    }
    if (partSize < fragm_)
    {
        partSize = fragm_;
        logger_.log(Logger::WARNING, "Partition size adjusted to " + std::to_string(partSize));
    }
    if (size_ > kMaxSize_)
    {
        size_ = kMaxSize_;
        logger_.log(Logger::WARNING, "Convolver size adjusted to " + std::to_string(size_));
    }
    if ((dens < 0.0f) || (dens > 1.0f))
    {
        logger_.log(Logger::ERROR, "Density parameter is out of range.");
        stat = ERR_PARAM;
    }

    convproc_.set_options(options_);
    int convReturnCode = convproc_.configure(ninp_, nout_, size_, fragm_, partSize, Convproc::MAXPART, dens);
    if (convReturnCode != CONVERROR::NO_ERROR)
    {
        logger_.log(Logger::ERROR, "Can't initialise convolution engine. Return code: " + std::to_string(convReturnCode));
        stat = ERR_INCONV;
    }

    if (stat == NOERR)
    {
        makeports();
        if (start_jclient())
            logError(ERR_OTHER);
        else
            allocOkay_ = true;
    }
    else
        logError(stat);
}

VaraysAuralizer::~VaraysAuralizer()
{
    jclient_.reset(nullptr); // Destroy jclient_ before destroying convproc_ and dopplerEngine_
}

VaraysAuralizer::ERROR_CODE VaraysAuralizer::updateIR(const std::string& sourceName, uint32_t nchan, const std::vector<float>& data)
{
    unsigned int length = data.size() / nchan;
    if (!jclient_->is_input_port(sourceName))
    {
        logError(ERR_IONUM);
        return ERR_IONUM;
    }
    for (unsigned int ichan = 0; ichan < nout_; ++ichan)
    {
        int convReturnCode = convproc_.impdata_update(sourceName, ichan, nchan, const_cast<float*>(&data[ichan]), 0, length);
        if (convReturnCode != CONVERROR::NO_ERROR)
        {
            logger_.log(Logger::Priority::ERROR, "Unable to update IR. Return code: " + std::to_string(convReturnCode));
            logError(ERR_INCONV);
            return ERR_INCONV;
        }
    }
    return NOERR;
}

VaraysAuralizer::ERROR_CODE VaraysAuralizer::updateDelayLines(const std::string& sourceName, const SoundEvent& directSoundEvent, bool sourceInsight, bool teleport)
{
    if (dopplerEnabled())
        dopplerEngine_->updateSource(sourceName, directSoundEvent, sourceInsight, teleport);
    else
        logger_.log(Logger::WARNING, "Doppler engine is not enabled.");

    return NOERR;
}

VaraysAuralizer::ERROR_CODE VaraysAuralizer::newSource(const std::string& sourceName)
{
    // create convolver for the input
    int convReturnCode = convproc_.input_create(sourceName);
    if (convReturnCode != CONVERROR::NO_ERROR)
    {
        logger_.log(Logger::ERROR, "Unable to create source. Return code: " + std::to_string(convReturnCode));
        logError(ERR_INCONV);
        return ERR_INCONV;
    }
    // create delay lines for doppler shift
    if (dopplerEnabled())
        dopplerEngine_->inputCreate(sourceName);

    // create jack input port if it doesn't exist already
    if (!jclient_->is_input_port(sourceName))
        jclient_->add_input_port(sourceName);
    return NOERR;
}

VaraysAuralizer::ERROR_CODE VaraysAuralizer::deleteSource(const std::string& sourceName)
{
    if (jclient_->is_input_port(sourceName))
    {
        if (jclient_->delete_port(sourceName))
            return ERR_OTHER;
    }
    int convReturnCode = convproc_.input_delete(sourceName);
    if (convReturnCode != CONVERROR::NO_ERROR)
    {
        logger_.log(Logger::ERROR, "Unable to delete source. Return code: " + std::to_string(convReturnCode));
        logError(ERR_INCONV);
        return ERR_INCONV;
    }
    // delete delay lines
    if (dopplerEnabled())
        dopplerEngine_->inputDelete(sourceName);
    return NOERR;
}

VaraysAuralizer::ERROR_CODE VaraysAuralizer::start_jclient()
{
    if (!jclient_)
    {
        logger_.log(Logger::ERROR, "Jack Client not initialized.");
        return ERR_OTHER;
    }
    jclient_->start();
    return NOERR;
}

void VaraysAuralizer::setVectorizedOperations(bool enableVectorization)
{
    if (enableVectorization)
        options_ |= Convproc::OPT_VECTOR_MODE;
    else
        options_ &= ~Convproc::OPT_VECTOR_MODE;
    convproc_.set_options(options_);
}

unsigned int VaraysAuralizer::getJClientFlags()
{
    return jclient_->flags();
}

VaraysAuralizer::ERROR_CODE VaraysAuralizer::check_inout(unsigned int ip, unsigned int op)
{
    ERROR_CODE stat = NOERR;
    if (!size_)
        stat = ERR_NOCONV;
    if ((ip < 1) || (ip > ninp_))
        stat = ERR_IONUM;
    if ((op < 1) || (op > nout_))
        stat = ERR_IONUM;
    logError(stat);
    return stat;
}

void VaraysAuralizer::makeports()
{
    // Create only the output jack ports. The input ports are to be dynamically created.
    for (size_t i = 0; i < nout_; ++i)
    {
        if (out_name_[i].empty())
        {
            jclient_->add_output_port("out-" + std::to_string(i));
        }
        else
        {
            jclient_->add_output_port(out_name_[i], out_conn_[i]);
        }
    }
}

void VaraysAuralizer::logError(ERROR_CODE err)
{
    switch (err)
    {
    case ERR_SYNTAX:
        logger_.log(Logger::ERROR, "Syntax Error.");
        break;
    case ERR_PARAM:
        logger_.log(Logger::ERROR, "Bad or missing parameters.");
        break;
    case ERR_ALLOC:
        logger_.log(Logger::ERROR, "Out of memory.");
        break;
    case ERR_CANTCD:
        logger_.log(Logger::ERROR, "Can't change directory.");
        break;
    case ERR_COMMAND:
        logger_.log(Logger::ERROR, "Unknown command.");
        break;
    case ERR_NOCONV:
        logger_.log(Logger::ERROR, "No convolver yet defined.");
        break;
    case ERR_INCONV:
        logger_.log(Logger::ERROR, "Error in convolver.");
        break;
    case ERR_IONUM:
        logger_.log(Logger::ERROR, "Bad input or output number.");
        break;
    default:
        logger_.log(Logger::ERROR, "Unknown error.");
    }
}
}; // namespace varays