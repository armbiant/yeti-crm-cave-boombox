#include "SoundEvent.hpp"

float SoundEvent::getEnergyForFrequency(float frequency) const
{
    if (frequency <= frequencies_.front())
        return energies_.front();
    else if (frequency >= frequencies_.back())
        return energies_.back();

    size_t frequenciesSize = frequencies_.size() - 1;

    for (size_t i = 0; i < frequenciesSize; ++i)
    {
        if (frequency >= frequencies_[i] && frequency < frequencies_[i + 1])
        {
            float fractionOfi = (frequencies_[i + 1] - frequency) / (frequencies_[i + 1] - frequencies_[i]);
            return fractionOfi * energies_[i] + (1.f - fractionOfi) * energies_[i + 1];
        }
    }
    return 0.f;
}

float SoundEvent::getPhaseForFrequency(float frequency, float speedOfSound) const
{
    float phase = frequency * (length_ / speedOfSound);
    phase -= floor(phase);
    phase *= 2.f * M_PI;
    if (phase > M_PI)
        phase = -2.f * M_PI + phase;
    return phase;
}

void SoundEvent::serialize(Json::Value& root) const
{
    Json::Value freqVector, energyVector, reflectionsVector, dirVec;

    root["sourceID_"] = sourceID_;
    root["length_"] = length_;

    for (size_t i = 0; i < frequencies_.size(); ++i)
    {
        freqVector.append(frequencies_[i]);
        energyVector.append(energies_[i]);
    }
    for (size_t i = 0; i < reflections_.size(); ++i)
    {
        reflectionsVector.append(reflections_[i]);
    }
    root["frequencies_"] = freqVector;
    root["energies_"] = energyVector;
    root["reflections_"] = reflectionsVector;

    dirVec.append(sphericalCoordinates_[0]);
    dirVec.append(sphericalCoordinates_[1]);
    dirVec.append(sphericalCoordinates_[2]);
    root["direction_"] = dirVec;
}

void SoundEvent::deserialize(const Json::Value& root)
{
    Json::Value freqVec, enerVec, reflecVec, dirVec;

    sourceID_ = root.get("sourceID_", 0).asInt();
    length_ = root.get("length_", 0.0f).asFloat();

    freqVec = root.get("frequencies_", 0);
    enerVec = root.get("energies_", 0);
    reflecVec = root.get("reflections_", 0);
    frequencies_.clear();
    energies_.clear();
    reflections_.clear();
    for (Json::ArrayIndex i = 0; i < freqVec.size(); ++i)
    {
        frequencies_.push_back(freqVec.get(i, 0.0f).asFloat());
        energies_.push_back(enerVec.get(i, 0.0f).asFloat());
    }
    for (Json::ArrayIndex i = 0; i < reflecVec.size(); ++i)
    {
        reflections_.push_back(Reflection(reflecVec.get(i, 0.0f).asInt()));
    }

    dirVec = root.get("direction_", 0);
    float dir1, dir2, dir3;
    dir1 = dirVec.get(static_cast<Json::ArrayIndex>(0), 0.0f).asFloat();
    dir2 = dirVec.get(static_cast<Json::ArrayIndex>(1), 0.0f).asFloat();
    dir3 = dirVec.get(static_cast<Json::ArrayIndex>(2), 0.0f).asFloat();
    sphericalCoordinates_ = Eigen::Vector3f(dir1, dir2, dir3);
}

bool SoundEvent::operator==(const SoundEvent& b) const
{
    bool freqAndEner = true;
    if (frequencies_.size() != b.frequencies_.size())
        return false;
    if (energies_.size() != b.energies_.size())
        return false;
    for (size_t i = 0; i < b.frequencies_.size(); ++i)
    {
        if (frequencies_[i] != b.frequencies_[i] || energies_[i] != b.energies_[i])
            return false;
    }
    return freqAndEner && (sourceID_ == b.sourceID_) && (length_ == b.length_) && (sphericalCoordinates_ == b.sphericalCoordinates_);
}
