// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "./AudioSource.hpp"

namespace varays
{
AudioSource::AudioSource(unsigned int id,
    const std::string& name,
    const Eigen::Vector3f& sourcePos,
    const std::vector<float>& frequencies,
    float energyFromRays)
    : id_(id)
    , name_(name)
    , position_(sourcePos)
{
    for (const auto& freq : frequencies)
        energy_[freq] = energyFromRays;
}

void AudioSource::setPos(const Eigen::Vector3f& sourcePos)
{
    position_ = sourcePos;
}

void AudioSource::setEnergy(float energy)
{
    for (auto it = energy_.begin(); it != energy_.end(); ++it)
        it->second = energy;
}

}; // namespace varays
