// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <cassert> 
#include <cmath>

#include "./AudioMat.hpp"

namespace varays
{
float AudioMat::getAbsorption(float frequency)
{
    auto absorptionIt = absorption_.find(frequency);
    assert(absorptionIt != absorption_.end());
    return absorptionIt->second;
}
float AudioMat::getTransmission(float frequency)
{
    auto transmissionIt = transmission_.find(frequency);
    assert(transmissionIt != transmission_.end());
    return transmissionIt->second;
}
float AudioMat::getScattering(float frequency)
{
    auto scatteringIt = scattering_.find(frequency);
    assert(scatteringIt != scattering_.end());
    return scatteringIt->second;
}
bool AudioMat::ChangeProperty(const std::string& property, float frequency, float value)
{
    if (property == "absorption")
    {
        auto it = absorption_.find(frequency);
        if (it == absorption_.end())
            return false;
        it->second = value;
        return true;
    }
    if (property == "transmission")
    {
        auto it = transmission_.find(frequency);
        if (it == transmission_.end())
            return false;
        it->second = value;
        return true;
    }
    if (property == "scattering")
    {
        auto it = scattering_.find(frequency);
        if (it == scattering_.end())
            return false;
        it->second = value;
        return true;
    }
    return false;
}
} // namespace varays