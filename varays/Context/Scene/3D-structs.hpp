// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef __VARAYS_3D_STRUCTS__
#define __VARAYS_3D_STRUCTS__

#include <Eigen/Dense>

namespace varays
{

struct Triangle
{
    int v0, v1, v2;
};

struct Basis
{
    Eigen::Vector3f vx;
    Eigen::Vector3f vy;
    Eigen::Vector3f vz;

    Basis(Eigen::Vector3f x, Eigen::Vector3f y, Eigen::Vector3f z)
        : vx(x)
        , vy(y)
        , vz(z)
    {
    }
};

static const Basis CanonicalBase = Basis(Eigen::Vector3f(1.0f, .0f, .0f), Eigen::Vector3f(.0f, 1.f, .0f), Eigen::Vector3f(.0f, .0f, 1.f));

struct TransformationMatrix
{
    Basis base_;
    Eigen::Vector3f trnsl;

    TransformationMatrix(const Basis b, const Eigen::Vector3f& t)
        : base_(b)
        , trnsl(t)
    {
    }
    TransformationMatrix(const Eigen::Vector3f& x, const Eigen::Vector3f& y, const Eigen::Vector3f& z, const Eigen::Vector3f& t)
        : base_(x, y, z)
        , trnsl(t)
    {
    }

    Eigen::Vector3f operator[](int vec) const
    {
        switch (vec)
        {
        case 0:
            return base_.vx;
            break;
        case 1:
            return base_.vy;
            break;
        case 2:
            return base_.vz;
            break;
        case 3:
            return trnsl;
            break;
        default:
            throw std::out_of_range("Argument out of range in TransformationMatrix[] operator");
        }
    }
};

static const TransformationMatrix NeutralTransformationMatrix = TransformationMatrix(CanonicalBase, Eigen::Vector3f(.0f, .0f, .0f));

}; // namespace varays

#endif
