// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "./EmbreeManager.hpp"
#include "./utils.hpp"
namespace varays
{
const float kMaxLength = 400.0f;

std::vector<utils::NodeInfo> EmbreeManager::ThrowRays(const std::vector<utils::RayToThrow>& originsAndDirections, RTCScene& scene)
{
    uint32_t size = originsAndDirections.size();
    auto rayHitArray = std::vector<RTCRayHit>(size);

    for (uint32_t i = 0; i < size; ++i)
    {
        RTCRayHit rayHit;

        // Beginning of the ray
        rayHit.ray.org_x = originsAndDirections[i].origin[0];
        rayHit.ray.org_y = originsAndDirections[i].origin[1];
        rayHit.ray.org_z = originsAndDirections[i].origin[2];

        // Direction
        rayHit.ray.dir_x = originsAndDirections[i].direction[0];
        rayHit.ray.dir_y = originsAndDirections[i].direction[1];
        rayHit.ray.dir_z = originsAndDirections[i].direction[2];

        // Range
        rayHit.ray.time = .0f;
        rayHit.ray.tnear = 0.00001f;
        rayHit.ray.tfar = kMaxLength;

        // Some default arguments
        rayHit.ray.flags = 0;
        rayHit.hit.instID[0] = RTC_INVALID_GEOMETRY_ID;
        rayHit.hit.geomID = RTC_INVALID_GEOMETRY_ID;

        rayHitArray[i] = rayHit;
    }
    RTCIntersectContext context;
    rtcIntersect1M(scene, &context, rayHitArray.data(), size, sizeof(RTCRayHit));
    std::vector<utils::NodeInfo> NodeInfo;
    NodeInfo.reserve(size);
    for (uint32_t i = 0; i < size; ++i)
    {
        // RTCRayHit.ray.tfar is updated after calling rtcIntersect
        // The value will be equivalent to minimum(origin value, distance from origin to contact point)
        // The ray hasn't hit any surface
        if (rayHitArray[i].ray.tfar == kMaxLength)
        {
            NodeInfo.emplace_back(utils::NodeInfo{
                .type = utils::NodeType::INVALID, 
                .coordinates = Eigen::Vector3f(0,0,0), 
                .normal = Eigen::Vector3f(0,0,0), 
                .length = 0, 
                .material = {}
                });
        }
        // The ray has hit a surface
        else
        {
            Eigen::Vector3f colPoint = originsAndDirections[i].origin + rayHitArray[i].ray.tfar * originsAndDirections[i].direction;
            Eigen::Vector3f normal = (Eigen::Vector3f(-rayHitArray[i].hit.Ng_x, -rayHitArray[i].hit.Ng_y, -rayHitArray[i].hit.Ng_z)).normalized();
            if (originsAndDirections[i].direction.dot(normal) > 0)
            {
                normal *= -1.f;
            }
            AudioMat* matPtr = static_cast<GeomData*>(rtcGetGeometryUserData(rtcGetGeometry(scene, rayHitArray[i].hit.geomID)))->getMat();
            NodeInfo.emplace_back(utils::NodeInfo{
                .type = utils::NodeType::REFLECTION, 
                .coordinates = colPoint, 
                .normal = normal, 
                .length = rayHitArray[i].ray.tfar, 
                .material = *matPtr});
        }
    }
    assert(originsAndDirections.size() == NodeInfo.size());
    return NodeInfo;
};

std::vector<std::vector<bool>> EmbreeManager::VisibilityTests(const std::vector<AudioSource>& sources, const std::vector<Eigen::Vector3f>& positions, RTCScene& scene)
{
    uint32_t sourcesSize = sources.size();
    uint32_t positionsSize = positions.size();
    std::vector<RTCRay> rayArray(sourcesSize * positionsSize);
    for (uint32_t i = 0; i < positionsSize; ++i)
    {
        for (uint32_t j = 0; j < sourcesSize; ++j)
        {
            uint32_t k = i * sourcesSize + j;
            RTCRay ray;

            // Beginning of the ray
            ray.org_x = positions[i][0];
            ray.org_y = positions[i][1];
            ray.org_z = positions[i][2];

            // Direction
            Eigen::Vector3f dir = sources[j].getPos() - positions[i];
            ray.dir_x = dir[0];
            ray.dir_y = dir[1];
            ray.dir_z = dir[2];

            ray.tnear = 0.00001f;
            ray.tfar = 1.0f;
            ray.flags = 0;

            rayArray[k] = ray;
        }
    }
    RTCIntersectContext context;
    rtcInitIntersectContext(&context);
    rtcOccluded1(scene, &context, rayArray.data());

    //[position][source]
    std::vector<std::vector<bool>> visibilityResults;
    visibilityResults.reserve(positionsSize);
    for (uint32_t i = 0; i < positionsSize; ++i)
    {
        std::vector<bool> visibilityForOrigin;
        visibilityForOrigin.reserve(sourcesSize);
        for (uint32_t j = 0; j < sourcesSize; ++j)
        {
            visibilityForOrigin.emplace_back(rayArray[i * sourcesSize + j].tfar == 1.0f);
        }
        visibilityResults.emplace_back(visibilityForOrigin);
    }
    return visibilityResults;
}
}; // namespace varays