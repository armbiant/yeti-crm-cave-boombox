// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef __VARAYS_UTILS__
#define __VARAYS_UTILS__

#include <vector>

#include <Eigen/Dense>
#include <embree3/rtcore.h>

#include "../../Common/AudioSource.hpp"
#include "./DiffractionData.hpp"
#include "./RootNode.hpp"


namespace varays
{
namespace utils
{

static const int Frequency = 44100;

enum NodeType
{
    INVALID = 0,
    REFLECTION = 1,
    DIFFRACTION = 2
};

// Contains the information necessary to create a TreeNode, is used for promises from the RayManager to the TaskManager
struct NodeInfo
{
    NodeType type;
    Eigen::Vector3f coordinates;
    Eigen::Vector3f normal;
    float length;
    AudioMat material;
};

// Specular and diffuse energy or energy ratios
struct SpecAndDiff
{
    float specular;
    float diffuse;
};

// Contains the information of a ray that is to be thrown
struct RayToThrow
{
    Eigen::Vector3f origin;
    Eigen::Vector3f direction;
    // Weight of specular and diffuse energy (from the node) this ray will carry.
    // The sum of both specular and diffuse energies for each ray thrown from a single node should always be exactly 1
    SpecAndDiff weights;
};

/**
* Generates the weighted random (Gaussian distribution) ray directions and energy weights from a reflection point
* The first direction will always correspond to the specular reflection
*
* @param origin Coordinates of the reflection point
* @param raydir Direction of the incoming ray
* @param normal Normal of the surface of the reflection point
* @param nbRay Amount of rays thrown from the reflection point
* @return Outgoing ray directions and energy weights from the reflection point
*/
std::vector<RayToThrow> reflectionDirections(const Eigen::Vector3f& origin, const Eigen::Vector3f& raydir, const Eigen::Vector3f& normal, int nbRay);

/**
* Applies a 180 degrees rotation of a spherical vector
* 
* @param sphericalVector Original spherical vector
* @return Rotated spherical vector
*/
Eigen::Vector3f reverseSphericalVector(Eigen::Vector3f sphericalVector);

/**
* Returns an equally distributed random vector3f within a sphere of radius 1
*/
Eigen::Vector3f ballRandom();

/**
*Returns an equally distributed random vector3f on the surface of a sphere of radius 1
*/
Eigen::Vector3f sphereRandom();

/**
* Calculate the initial energy of each ray for every source
* 
* @param raysToThrow Amount of rays thrown from the listened
* @param parentEnergies Total energy emitted for each source
* @return Energy emitted from each ray, for each source
*/
std::map<int, varays::energy> initialRayEnergy(int raysToThrow, const std::map<int, varays::energy>& parentEnergies);

/**
* Calculate the energy reflected for a new reflection ray.
* 
* @param raySent Information on the new ray to send from the reflection point
* @param parentEnergies Energy contained by the ray that hit the reflection point
* @return Energy containted by the new ray to send, for each source
*/
std::map<int, varays::energy> reflectedRayEnergy(const RayToThrow& raySent, const std::map<int, Energies>& parentEnergies);

/**
* Make a TreeNode for a reflection point
* 
* @param newNodeInfo Information about the reflection surface and the incoming ray
* @param origin Coordinates of the origin of the incoming ray
* @param incomingEnergies Energy carried by the incoming ray
* @param parentLength Total length traveled until the beginning of the incoming ray
* @param reflectionTypes Types of reflections for the children of this node. (Typically 1 specular reflection and x diffuse ones)
* @return TreeNode for a reflection point
*/
TreeNode makeNode(NodeInfo& newNodeInfo,
    const Eigen::Vector3f& origin,
    const std::map<int, varays::energy>& incomingEnergies,
    float parentLength,
    const std::vector<SoundEvent::Reflection>& reflectionTypes);

/**
* Creates the soundEvents in a treeNode from the visibility test results
* 
* @param visibilityResults Visibility test results for each source for a specific treeNode
* @param sources List of sources for which the visibility tests were conducted
* @param newNode TreeNode for which the soundEvents will be constructed
* @return Copy of the treeNode given as an input, but with soundEvents added
*/
void computeSoundEvents(const std::vector<bool>& visibilityResults, const std::vector<AudioSource>& sources, TreeNode& newNode);

/**
* Calculates the solid angle that corresponds to the visibility of a source from a point
* 
* @param radius Radius of the source
* @param origin Point from which the visibility angle will be calculated
* @param source Coordinates of the source
* @return Solid angle proportion for a source (used for direct rays)
*/
float visibilityAngleProportion(float radius, const Eigen::Vector3f& origin, const Eigen::Vector3f& source);

/**
* Calculates the amount of energy that reaches a specific source from a reflection node
* 
* @param radius Radius of the source
* @param origin Coordinates of the node
* @param source Coordinates of the source
* @param raydir Direction of the incoming ray on the surface
* @param normal Normal of the surface of the node
* @return Proportion of specular and proportion of diffuse energy that reaches the source from one reflection point
*/
SpecAndDiff energyProportionToSource(float radius, const Eigen::Vector3f& origin, const Eigen::Vector3f& source, const Eigen::Vector3f& raydir, const Eigen::Vector3f& normal);

}; // namespace utils
}; // namespace varays

#endif
