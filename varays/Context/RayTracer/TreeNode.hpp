// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef __VARAYS_TREE_NODE__
#define __VARAYS_TREE_NODE__

#include <vector>

#include <Eigen/Dense>

#include "../../Common/AudioMat.hpp"
#include "../../Common/SoundEvent.hpp"
#include "../../Common/Energy.hpp"

namespace varays
{
// A TreeNode is an intersection point in the rayTree. It has a location which should always be a on a surface.
struct TreeNode
{
    Eigen::Vector3f coordinates;
    Eigen::Vector3f rayDirection; // spherical coordinates
    Eigen::Vector3f normal; // Normal of the surface the node is on
    float length; // Represents the total length traveled by the last ray and its parents to get to this intersection point.
    Eigen::Vector3f firstRayDirection; // Direction of the first "root" ray leaving the listener
    // <source ID<specular<frequency, energy>, diffuse<frequency, energy>>>
    // With the absorption and transmission from this material already applied
    std::map<int, varays::Energies> outgoingEnergies;
    AudioMat audioMat;
    std::vector<SoundEvent::Reflection> reflectionTypes;

    std::vector<TreeNode> children{};
    std::vector<SoundEvent> soundEvents{};
};
}; // namespace varays
#endif //__VARAYS_TREE_NODE__