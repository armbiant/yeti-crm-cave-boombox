#ifndef __JSON_SAVE__
#define __JSON_SAVE__

#include <string>
#include <vector>

#include <jsoncpp/json/json.h>

class JsonSerializable
{
  public:
    virtual ~JsonSerializable(){};
    virtual void serialize(Json::Value& root) const = 0;
    virtual void deserialize(const Json::Value& root) = 0;
    virtual std::string type_string() const = 0;
};

// Serialize object of type T into a string. Class T must inherit from JsonSerializable.
template <class T, typename std::enable_if<std::is_base_of<JsonSerializable, T>::value>::type* = nullptr> bool jsonSerialize(const T* pObj, std::string& output)
{
    if (pObj)
        return false;

    Json::Value serializeRoot;
    serializeRoot["ObjectType"] = pObj->type_name();
    pObj->serialize(serializeRoot);

    Json::StyledWriter writer;
    output = writer.write(serializeRoot);

    return true;
}

// Deserialize string into object of type T. Class T must inherit from JsonSerializable.
template <class T, typename std::enable_if<std::is_base_of<JsonSerializable, T>::value>::type* = nullptr>
bool jsonDeserialize(T* pObj, const std::string& input)
{
    if (pObj == nullptr)
        return false;

    Json::Value deserializeRoot;
    Json::Reader reader;

    if (!reader.parse(input, deserializeRoot))
        return false;

    // Check if the deserialized data is of type T
    std::string objType = deserializeRoot.get("ObjectType", "Not Defined").asString();
    if (objType != pObj->type_string())
        return false;

    pObj->deserialize(deserializeRoot);

    return true;
}

// Serialize vector of objects of type T into a string. Class T must inherit from JsonSerializable.
template <class T, typename std::enable_if<std::is_base_of<JsonSerializable, T>::value>::type* = nullptr>
bool jsonSerialize(const std::vector<T>& pVec, std::string& output)
{
    if (pVec.size() == 0)
        return false;

    Json::Value serializeVector, serializeRoot;

    int numElem = pVec.size();
    for (const auto& it : pVec) // iterate through the vector and serialize each object
    {
        Json::Value serializedElement;
        it.serialize(serializedElement);
        serializeVector.append(serializedElement);
    }
    serializeRoot["VectorType"] = pVec.begin()->type_string();
    serializeRoot["Size"] = numElem;
    serializeRoot["Vector"] = serializeVector;

    Json::StyledWriter writer;
    output = writer.write(serializeRoot);

    return true;
}

// Deserialize string into a vector of objects of type T. Class T must inherit from JsonSerializable.
template <class T, typename std::enable_if<std::is_base_of<JsonSerializable, T>::value>::type* = nullptr>
bool jsonDeserialize(std::vector<T>& pVec, const std::string& input)
{
    if (input.empty())
        return false;

    Json::Value deserializeRoot;
    Json::Reader reader;

    if (!reader.parse(input, deserializeRoot))
        return false;

    std::string vecType = deserializeRoot.get("VectorType", "Not Defined").asString();

    // check if the class types match
    T testObj;
    if (vecType != testObj.type_string())
        return false; // types don't match

    int numElem = deserializeRoot.get("Size", 0).asInt();
    Json::Value deserializeVector = deserializeRoot.get("Vector", 0);
    for (int i = 0; i < numElem; ++i)
    {
        T newObj;
        newObj.deserialize(deserializeVector[i]);
        pVec.push_back(newObj);
    }
    return true;
}

#endif //__JSON_SAVE__