// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <spdlog/sinks/stdout_color_sinks.h>

#include "./Logger.hpp"

namespace varays
{
Logger::Logger()
    : Logger("Unknown")
{
}

Logger::Logger(const std::string& callingClass)
{
    logger_ = spdlog::get(callingClass);
    if (!logger_)
        logger_ = spdlog::stdout_color_st(callingClass);
    logger_->set_pattern("[%^%n%$] %v");

#ifdef NDEBUG
            logger_->enable_backtrace(10);
#endif
}

void Logger::log(Priority type, std::string message) const
{
    switch (type)
    {
    case Priority::INFO:
        logger_->info(message);
        break;
    case Priority::WARNING:
        logger_->warn(message);
        break;
    case Priority::ERROR:
        logger_->error(message);
        break;
    case Priority::CRITICAL:
        logger_->critical(message);
        logger_->dump_backtrace();
        break;
    case Priority::DEBUG:
#ifndef NDEBUG
        logger_->debug(message);
#endif
        break;
    case Priority::TRACE:
        logger_->trace(message);
        break;
    default:
        logger_->log(spdlog::level::off, "Unknown log level - " + message);
    }
}
} // namespace varays
