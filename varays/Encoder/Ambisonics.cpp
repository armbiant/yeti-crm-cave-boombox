// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "./Ambisonics.hpp"

namespace HOA
{

std::vector<double> getAmbi(float theta, float phi, int order)
{
    std::vector<double> ambi;
    // Ambi order 0
    ambi.push_back(1.0f);
    if (order == 0)
        return ambi;

    double ux = cos(theta) * cos(phi);
    double uy = sin(theta) * cos(phi);
    double uz = sin(phi);

    // Ambi order 1
    ambi.push_back(uy);
    ambi.push_back(uz);
    ambi.push_back(ux);
    if (order == 1)
        return ambi;

    // powers of 2 to facilitate (and optimize) calculations
    double ux2 = pow(ux, 2.0);
    double uy2 = pow(uy, 2.0);
    double uz2 = pow(uz, 2.0);

    // order 2
    ambi.push_back(1.73205078 * ux * uy);
    ambi.push_back(1.73205078 * uy * uz);
    ambi.push_back(0.5 * (2.0 * uz2 - ux2 - uy2));
    ambi.push_back(1.73205078 * ux * uz);
    ambi.push_back(0.86602538824081421 * (ux2 - uy2));
    if (order == 2)
        return ambi;

    // order 3
    ambi.push_back(0.79056941504 * uy * (3.0 * ux2 - uy2));
    ambi.push_back(3.87298334621 * uz * ux * uy);
    ambi.push_back(0.61237243569 * uy * (4.0 * uz2 - ux2 - uy2));
    ambi.push_back(0.5 * uz * (2.0 * uz2 - 3.0 * ux2 - 3.0 * uy2));
    ambi.push_back(0.61237243569 * ux * (4.0 * uz2 - ux2 - uy2));
    ambi.push_back(1.9364916731 * uz * (ux2 - uy2));
    ambi.push_back(0.79056941504 * ux * (ux2 - 3.0 * uy2));
    if (order == 3)
        return ambi;

    // powers of 4
    float ux4 = pow(ux, 4.0);
    float uy4 = pow(uy, 4.0);
    float uz4 = pow(uz, 4.0);

    // order 4
    ambi.push_back(2.9580399990081787 * ux * uy * (ux2 - uy2));
    ambi.push_back(2.0916500091552734 * uy * uz * (3.0 * ux2 - uy2));
    ambi.push_back(1.1180340051651001 * ux * uy * (6.0 * uz2 - ux2 - uy2));
    ambi.push_back(0.79056942462921143 * uy * uz * (4.0 * uz2 - 3.0 * ux2 - 3.0 * uy2));
    ambi.push_back(0.125 * (8.0 * uz4 + 3.0 * ux4 + 3.0 * uy4 - 24.0 * ux2 * uz2 - 24.0 * uy2 * uz2 + 6.0 * ux2 * uy2));
    ambi.push_back(0.79056942462921143 * ux * uz * (4.0 * uz2 - 3.0 * ux2 - 3.0 * uy2));
    ambi.push_back(0.55901700258255005 * (ux2 - uy2) * (6.0 * uz2 - ux2 - uy2));
    ambi.push_back(2.0916500091552734 * (ux2 - 3.0 * uy2) * ux * uz);
    ambi.push_back(0.73950999975204468 * (ux4 - 6.0 * ux2 * uy2 + uy4));
    if (order == 4)
        return ambi;

    // order 5
    ambi.push_back(0.70156078040599823 * uy * (5.0 * ux4 - 10.0 * ux2 * uy2 + uy4));
    ambi.push_back(8.8741199970245361 * ux * uy * uz * (ux2 - uy2));
    ambi.push_back(0.52291250228881836 * uy * (uy4 - 2.0 * ux2 * uy2 - 3.0 * ux4 - 8.0 * uy2 * uz2 + 24.0 * ux2 * uz2));
    ambi.push_back(5.1234755516052246 * ux * uy * uz * (2.0 * uz2 - ux2 - uy2));
    ambi.push_back(0.48412293195724487 * uy * (ux4 + 2.0 * ux2 * uy2 + uy4 - 12.0 * ux2 * uz2 - 12.0 * uy2 * uz2 + 8.0 * uz4));
    ambi.push_back(0.125 * uz * (15.0 * ux4 + 15.0 * uy4 + 8.0 * uz4 + 30.0 * ux2 * uy2 - 40.0 * ux2 * uz2 - 40.0 * uy2 * uz2));
    ambi.push_back(0.48412293195724487 * ux * (ux4 + 2.0 * ux2 * uy2 + uy4 - 12.0 * ux2 * uz2 - 12.0 * uy2 * uz2 + 8.0 * uz4));
    ambi.push_back(2.5617377758026123 * uz * (2.0 * ux2 * uz2 - 2.0 * uy2 * uz2 - ux4 + uy4));
    ambi.push_back(0.52291250228881836 * ux * (2.0 * ux2 * uy2 + 8.0 * ux2 * uz2 - 24.0 * uy2 * uz2 - ux4 + 3.0 * uy4));
    ambi.push_back(2.218529999256134 * uz * (ux4 - 6.0 * ux2 * uy2 + uy4));
    ambi.push_back(0.70156078040599823 * ux * (ux4 - 10.0 * ux2 * uy2 + 5.0 * uy4));
    if (order == 5)
        return ambi;

    // powers of 6
    float ux6 = pow(ux, 6.0);
    float uy6 = pow(uy, 6.0);
    float uz6 = pow(uz, 6.0);

    // order 6
    ambi.push_back(1.3433865308761597 * ux * uy * (3.0 * ux4 - 10.0 * ux2 * uy2 + 3.0 * uy4));
    ambi.push_back(2.326813817024231 * uy * uz * (5.0 * ux4 - 10.0 * ux2 * uy2 + uy4));
    ambi.push_back(1.984313428401947 * ux * uy * (-ux4 + uy4 + 10.0 * ux2 * uz2 - 10.0 * uy2 * uz2));
    ambi.push_back(0.90571105480194092 * uy * uz * (-9.0 * ux4 + 3.0 * uy4 - 6.0 * ux2 * uy2 + 24.0 * ux2 * uz2 - 8.0 * uy2 * uz2));
    ambi.push_back(0.90571105480194092 * ux * uy * (ux4 + uy4 + 16.0 * uz4 + 2.0 * ux2 * uy2 - 16.0 * ux2 * uz2 - 16.0 * uy2 * uz2));
    ambi.push_back(0.5728219747543335 * uy * uz * (5.0 * ux4 + 5.0 * uy4 + 8.0 * uz4 + 10.0 * ux2 * uy2 - 20.0 * ux2 * uz2 - 20.0 * uy2 * uz2));
    ambi.push_back(0.0625 * (16 * uz6 - 5.0 * ux6 - 5.0 * uy6 - 15.0 * ux4 * uy2 + 90.0 * ux4 * uz2 + 90.0 * uy4 * uz2 - 120.0 * uy2 * uz4 - 15.0 * ux2 * uy4 -
                                120.0 * ux2 * uz4 + 180.0 * ux2 * uy2 * uz2));
    ambi.push_back(0.5728219747543335 * ux * uz * (5.0 * ux4 + 5.0 * uy4 + 8.0 * uz4 + 10.0 * ux2 * uy2 - 20.0 * ux2 * uz2 - 20.0 * uy2 * uz2));
    ambi.push_back(0.45285552740097046 * (ux6 - uy6 + ux4 * uy2 - ux2 * uy4 - 16.0 * ux4 * uz2 + 16.0 * ux2 * uz4 + 16.0 * uy4 * uz2 - 16.0 * uy2 * uz4));
    ambi.push_back(0.90571105480194092 * ux * uz * (-3.0 * ux4 + 6.0 * ux2 * uy2 + 8.0 * ux2 * uz2 - 24.0 * uy2 * uz2 + 9.0 * uy4));
    ambi.push_back(0.49607835710048676 * (-ux6 + 5.0 * ux4 * uy2 + 10.0 * ux4 * uz2 + 5.0 * ux2 * uy4 + 10.0 * uy4 * uz2 - uz6 - 60.0 * ux2 * uy2 * uz2));
    ambi.push_back(2.326813817024231 * ux * uz * (ux4 - 10.0 * ux2 * uy2 + 5.0 * uy4));
    ambi.push_back(0.5484352707862854 * (ux6 - 15.0 * ux4 * uy2 + 15.0 * ux2 * uy4 - uy6));
    if (order == 6)
        return ambi;
    return ambi;
}

}; // namespace HOA