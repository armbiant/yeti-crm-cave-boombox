// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
#ifndef __VARAYS_AMBISONICS__
#define __VARAYS_AMBISONICS__

#include <cmath>
#include <list>
#include <vector>

namespace HOA
{
// The ambisonic coefficients follow SN3D with Ambisonic Channel Numbering (ACN)
std::vector<double> getAmbi(float theta, float phi, int order);
}; // namespace HOA

#endif // __VARAYS_AMBISONICS__