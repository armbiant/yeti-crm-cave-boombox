// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <algorithm>

#include "LRTree.hpp"

namespace varays
{

void LRTree::LR_stage::resize(size_t numRows, size_t numCols)
{
    numFilters_ = numRows;
    numSections_ = numCols;
    b0_.resize(numRows, numCols);
    b1_.resize(numRows, numCols);
    b2_.resize(numRows, numCols);
    a0_.resize(numRows, numCols);
    a1_.resize(numRows, numCols);
    a2_.resize(numRows, numCols);
    s1_.resize(numRows, numCols);
    s2_.resize(numRows, numCols);

    clearStates();
}

void LRTree::LR_stage::clearStates()
{
    s1_.setZero();
    s2_.setZero();
}

void LRTree::LR_stage::resetCoefficients()
{
    if (numSections_ == 0)
        return;

    b0_.setOnes();
    b1_.setZero();
    b2_.setZero();
    a0_.setOnes();
    a1_.setZero();
    a2_.setZero();

    clearStates();
}

LRTree::LRTree(const std::vector<float>& bandFreqs, int fs)
    : centreFrequencies_(bandFreqs)
    , fs_(fs)
{
    if (centreFrequencies_.size() != 0 && (centreFrequencies_.size() & (centreFrequencies_.size() - 1)) != 0)
    {
        // number of bands not a power of 2
        logger_.log(Logger::ERROR, "Number of bands is not a power of 2");
        return;
    }
    numStages_ = std::log2(centreFrequencies_.size());
    filterStages_.resize(numStages_ + 1);

    computeFilterCoefficients();
}

void LRTree::process(Eigen::Ref<Eigen::ArrayXd> inFrame)
{
    if (inFrame.size() != filterStages_.begin()->numFilters_)
    {
        logger_.log(Logger::ERROR, "Incorrect number of rows in input");
        return;
    }

    for (unsigned int stage = 0; stage <= numStages_; ++stage)
    {
        for (unsigned int filt = 0; filt < filterStages_[stage].numFilters_; ++filt)
        {
            for (unsigned int sect = 0; sect < filterStages_[stage].numSections_; ++sect)
            {
                auto currentInFrame = inFrame(filt);
                inFrame(filt) = inFrame(filt) * filterStages_[stage].b0_(filt, sect) + filterStages_[stage].s1_(filt, sect);
                filterStages_[stage].s1_(filt, sect) = currentInFrame * filterStages_[stage].b1_(filt, sect) -
                                                       inFrame(filt) * filterStages_[stage].a1_(filt, sect) + filterStages_[stage].s2_(filt, sect);
                filterStages_[stage].s2_(filt, sect) =
                    currentInFrame * filterStages_[stage].b2_(filt, sect) - inFrame(filt) * filterStages_[stage].a2_(filt, sect);
            }
        }
        if (stage != numStages_)
        {
            size_t numIter = 1 << (numStages_ - stage - 1);
            for (size_t j = 0; j < numIter; ++j)
                inFrame(j) = inFrame(2 * j) + inFrame(2 * j + 1);
        }
    }
}

void LRTree::processBlock(Eigen::Ref<Eigen::Array<double, -1, -1, Eigen::RowMajor>> inBlock)
{
    if (inBlock.rows() != filterStages_.begin()->numFilters_)
    {
        logger_.log(Logger::ERROR, "Incorrect number of rows in input");
        return;
    }

    for (unsigned int stage = 0; stage <= numStages_; ++stage)
    {
        for (unsigned int filt = 0; filt < filterStages_[stage].numFilters_; ++filt)
        {
            for (unsigned int sect = 0; sect < filterStages_[stage].numSections_; ++sect)
            {
                for (int samp = 0; samp < inBlock.cols(); ++samp)
                {
                    auto currentInSamp = inBlock(filt, samp);
                    inBlock(filt, samp) = inBlock(filt, samp) * filterStages_[stage].b0_(filt, sect) + filterStages_[stage].s1_(filt, sect);
                    filterStages_[stage].s1_(filt, sect) = currentInSamp * filterStages_[stage].b1_(filt, sect) -
                                                           inBlock(filt, samp) * filterStages_[stage].a1_(filt, sect) + filterStages_[stage].s2_(filt, sect);
                    filterStages_[stage].s2_(filt, sect) =
                        currentInSamp * filterStages_[stage].b2_(filt, sect) - inBlock(filt, samp) * filterStages_[stage].a2_(filt, sect);
                }
            }
        }

        // add adjacent channels and save in the top half of the 2D array
        if (stage != numStages_)
        {
            size_t numIter = 1 << (numStages_ - stage - 1);
            for (size_t j = 0; j < numIter; ++j)
                inBlock.row(j) = inBlock.row(2 * j) + inBlock.row(2 * j + 1);
        }
    }
}

void LRTree::resetFilters()
{
    for (auto& fStage : filterStages_)
        fStage.clearStates();
}

int LRTree::getNumInputs()
{
    if (!filterStages_.empty())
        return filterStages_[0].numFilters_;
    return 0;
}

void LRTree::setSampleRate(int fs)
{
    fs_ = fs;
    for (auto& fStage : filterStages_)
        fStage.resetCoefficients();
    computeFilterCoefficients();
}

void LRTree::computeFilterCoefficients()
{
    std::vector<std::array<double, 6>> sosCoeffs_hpf(centreFrequencies_.size() - 1, std::array<double, 6>({0.0f}));
    std::vector<std::array<double, 6>> sosCoeffs_lpf(centreFrequencies_.size() - 1, std::array<double, 6>({0.0f}));
    for (size_t i = 0; i < centreFrequencies_.size() - 1; ++i)
    {
        float f_cutoff = std::sqrt(centreFrequencies_[i] * centreFrequencies_[i + 1]);
        computeCoefficients_2ndOrder(fs_, f_cutoff, sosCoeffs_lpf[i], sosCoeffs_hpf[i]);
    }

    for (unsigned int stage = 0; stage <= numStages_; ++stage)
    {
        // Number of poles/zeros in filters of each stage is 2 + 2 * (2^stage - 1),
        // with 2 being the number of poles/zeros of its own filter and
        // 2 * (2^stage - 1) being the poles/zeros of the allpass filters corresponding to the filters in the other child branch
        if (stage == numStages_)
            filterStages_[stage].resize(1 << (numStages_ - stage), 2);
        else
            filterStages_[stage].resize(1 << (numStages_ - stage), 2 + (1 << stage) - 1);
        filterStages_[stage].resetCoefficients();
        for (size_t i = 0; i < std::max(filterStages_[stage].numFilters_ / 2, static_cast<unsigned int>(1)); ++i)
        {
            size_t fc = static_cast<size_t>((1 << stage) - 1) + i * static_cast<size_t>(2 << stage);
            if (stage != numStages_)
            {
                // Copy numerator coefficients of the filters
                // No numerator coefficients to be copied for the output filter
                filterStages_[stage].b0_(2 * i, 0) = filterStages_[stage].b0_(2 * i, 1) = sosCoeffs_lpf[fc][0];
                filterStages_[stage].b1_(2 * i, 0) = filterStages_[stage].b1_(2 * i, 1) = sosCoeffs_lpf[fc][1];
                filterStages_[stage].b2_(2 * i, 0) = filterStages_[stage].b2_(2 * i, 1) = sosCoeffs_lpf[fc][2];
                filterStages_[stage].b0_(2 * i + 1, 0) = filterStages_[stage].b0_(2 * i + 1, 1) = sosCoeffs_hpf[fc][0];
                filterStages_[stage].b1_(2 * i + 1, 0) = filterStages_[stage].b1_(2 * i + 1, 1) = sosCoeffs_hpf[fc][1];
                filterStages_[stage].b2_(2 * i + 1, 0) = filterStages_[stage].b2_(2 * i + 1, 1) = sosCoeffs_hpf[fc][2];
            }
            if (stage != 0)
            {
                // Copy lpf denominator coefficients of the previous stage
                // No denominator coefficients to be copied for filters in the first stage (input filters)
                // No highpass filter denominator coefficients for the output filter
                filterStages_[stage].a0_(2 * i, 0) = filterStages_[stage].a0_(2 * i, 1) = sosCoeffs_lpf[fc - (1 << (stage - 1))][3];
                filterStages_[stage].a1_(2 * i, 0) = filterStages_[stage].a1_(2 * i, 1) = sosCoeffs_lpf[fc - (1 << (stage - 1))][4];
                filterStages_[stage].a2_(2 * i, 0) = filterStages_[stage].a2_(2 * i, 1) = sosCoeffs_lpf[fc - (1 << (stage - 1))][5];

                if (stage != numStages_)
                {
                    filterStages_[stage].a0_(2 * i + 1, 0) = filterStages_[stage].a0_(2 * i + 1, 1) = sosCoeffs_lpf[fc + (1 << (stage - 1))][3];
                    filterStages_[stage].a1_(2 * i + 1, 0) = filterStages_[stage].a1_(2 * i + 1, 1) = sosCoeffs_lpf[fc + (1 << (stage - 1))][4];
                    filterStages_[stage].a2_(2 * i + 1, 0) = filterStages_[stage].a2_(2 * i + 1, 1) = sosCoeffs_lpf[fc + (1 << (stage - 1))][5];

                    // Copy allpass filter coefficients of the respective opposite branch filters
                    // No allpass filters in the output filter
                    for (size_t a = 1, s = 2; a <= std::pow(2, stage) - 1; ++a)
                    {
                        // APF coefficients of upper branch
                        filterStages_[stage].b0_(2 * i, s) = sosCoeffs_lpf[fc + a][5];
                        filterStages_[stage].b1_(2 * i, s) = sosCoeffs_lpf[fc + a][4];
                        filterStages_[stage].b2_(2 * i, s) = sosCoeffs_lpf[fc + a][3];
                        filterStages_[stage].a0_(2 * i, s) = sosCoeffs_lpf[fc + a][3];
                        filterStages_[stage].a1_(2 * i, s) = sosCoeffs_lpf[fc + a][4];
                        filterStages_[stage].a2_(2 * i, s) = sosCoeffs_lpf[fc + a][5];

                        // APF coefficients of lower branch
                        filterStages_[stage].b0_(2 * i + 1, s) = sosCoeffs_lpf[fc - a][5];
                        filterStages_[stage].b1_(2 * i + 1, s) = sosCoeffs_lpf[fc - a][4];
                        filterStages_[stage].b2_(2 * i + 1, s) = sosCoeffs_lpf[fc - a][3];
                        filterStages_[stage].a0_(2 * i + 1, s) = sosCoeffs_lpf[fc - a][3];
                        filterStages_[stage].a1_(2 * i + 1, s) = sosCoeffs_lpf[fc - a][4];
                        filterStages_[stage].a2_(2 * i + 1, s) = sosCoeffs_lpf[fc - a][5];

                        ++s;
                    }
                }
            }
        }
    }
}

void LRTree::computeCoefficients_2ndOrder(int fs, float freq, std::array<double, 6>& sosCoeffs_lpf, std::array<double, 6>& sosCoeffs_hpf)
{
    double k = std::tan(M_PI * static_cast<double>(freq) / static_cast<double>(fs));
    double k_sq = std::pow(k, 2.0);
    double den = 1 + M_SQRT2 * k + k_sq;

    // compute denominator coefficients of the LPF and HPF.
    sosCoeffs_lpf[3] = sosCoeffs_hpf[3] = 1.0;
    sosCoeffs_lpf[4] = sosCoeffs_hpf[4] = 2 * (k_sq - 1) / den;
    sosCoeffs_lpf[5] = sosCoeffs_hpf[5] = (1 - M_SQRT2 * k + k_sq) / den;

    // compute numerator coefficients of the LPF.
    sosCoeffs_lpf[0] = k_sq / den;
    sosCoeffs_lpf[1] = 2.0 * sosCoeffs_lpf[0];
    sosCoeffs_lpf[2] = sosCoeffs_lpf[0];

    // compute numerator coefficients of the HPF.
    sosCoeffs_hpf[0] = 1.0 / den;
    sosCoeffs_hpf[1] = -2.0 * sosCoeffs_hpf[0];
    sosCoeffs_hpf[2] = sosCoeffs_hpf[0];
}

}; // namespace varays