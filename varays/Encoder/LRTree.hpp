// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef __LRTREE__
#define __LRTREE__

#include <fstream>
#include <string>
#include <vector>

#include <Eigen/Dense>
#include <jsoncpp/json/json.h>

#include "../Utils/Logger.hpp"

namespace varays
{

/*
 * Linkwitz-Riley Filter Tree.
 *
 * The Linkwitz-Riley filter is basically a combination of a lowpass filter and a highpass filter
 * with the same cutoff frequency, whose magnitude sum crossover is 0dB. The Linkwitz-Riley filter
 * tree is a set of these crossover filters splitting the frequency spectrum into bands, whose sum
 * is a flat reponse. In the vaRays context, we use the LR filter tree to essentially synthesize
 * a full spectrum room impulse response from impulse responses of the individual frequency bands.
 */
class LRTree
{
  public:
    /*
     * Constructor
     *
     * Setup filters in the Linkwitz-Riley filter tree.
     *
     * @param  bandFreqs  Centre frequencies of filter bands in Hz
     * @param  fs         Sample rate of operation in Hz
     */
    LRTree(const std::vector<float>& bandFreqs, int fs);

    /*
     * Processes a single frame of input samples.
     *
     * This function filters a single frame of input samples in place.
     *
     * @param  inFrame An Eigen::ArrayXd of size (num input filters, 1)
     */
    void process(Eigen::Ref<Eigen::ArrayXd> inFrame);

    /*
     * Processes a block of input samples.
     *
     * This function filters a block input samples in place.
     *
     * @param  inBlock  An row major Eigen::Array of size (num input filters, block size)
     */
    void processBlock(Eigen::Ref<Eigen::Array<double, -1, -1, Eigen::RowMajor>> inBlock);

    /* Reset states of all filters. */
    void resetFilters();

    /*
     * Return number of input filters/channels.
     *
     * @return numInputs Number of input channels (= Number of filters in the 1st stage)
     */
    int getNumInputs();

    /*
     * Return the sample rate.
     *
     * @return fs Sample rate in Hz.
     */
    int getSampleRate() { return fs_; }

    /*
     * Set sample rate of operation.
     *
     * This function sets the sample rate of the filter and recomputes the filter coefficients.
     *
     * @param  fs  new sample rate
     */
    void setSampleRate(int fs);

    /*
     * Returns the centre frequencies of the Linkwitz-Riley filters.
     *
     * The centre frequencies of the filters are the square root of the product of adjacent crossover frequencies.
     *
     * @return centreFrequencies vector of centre frequencies in Hz.
     */
    std::vector<float> getCentreFrequencies() { return centreFrequencies_; };

  private:
    /*
     * A single stage of the Linkwitz-Riley filter tree.
     *
     * This struct stores the coefficients and states of a single stage of LR filters
     * which a set of cascaded biquad filters (second order sections) in parallel.
     */
    struct LR_stage
    {
        Eigen::Array<double, -1, -1> b0_, b1_, b2_; /**< Numerator Coefficients */
        Eigen::Array<double, -1, -1> a0_, a1_, a2_; /**< Denominator Coefficients */
        Eigen::Array<double, -1, -1> s1_, s2_; /**< Filter states */

        unsigned int numFilters_{0}; /**< Number of filters in parallel */
        unsigned int numSections_{0}; /**< Number of second order sections in each filter */

        /*
         * Set the number of filters in parallel and number of biquad section.
         *
         * This function sets the number filters and section in a stage of the LR filter tree
         * by resizing the coefficient and state arrays. This function is called before setting coefficients.
         *
         * @param  numRows Number of filters in parallel
         * @param  numCols Number of biquad sections in each filter
         */
        void resize(size_t numRows, size_t numCols);

        /*
         * Clear filter states.
         */
        void clearStates();

        /*
         * Reset all filter coefficients to 0.
         */
        void resetCoefficients();
    };

    std::vector<LR_stage> filterStages_; /**< Vector of LR filter stages */
    std::vector<float> centreFrequencies_; /**< Centre frequencies of the filter bands. */
    int fs_; /**< Sample rate */
    unsigned int numStages_{0}; /**< Number of LR filter stages */

    Logger logger_{"LRTree"};

    /*
     * Compute coefficients of all filter in the LR filter tree.
     *
     * This function computes the filter coefficients for all biquad sections in the LR tree
     * with the given centre frequencies and sample rate.
     */
    void computeFilterCoefficients();

    /*
     * Compute the 2nd order Lowpass and Highpass IIR filter coefficients.
     *
     * @param[in]  fs             Sample rate
     * @param[in]  freq           Critical frequency of the filter (Crossover frequency of an LR filter)
     * @param[out] sosCoeffs_lpf  2nd order LPF coefficients
     * @param[out] sosCoeffs_hpf  2nd order HPF coefficients
     */
    void computeCoefficients_2ndOrder(int fs, float freq, std::array<double, 6>& sosCoeffs_lpf, std::array<double, 6>& sosCoeffs_hpf);
};
}; // namespace varays

#endif // __LRTREE__