add_definitions(-Wno-deprecated-copy)

include_directories(${CMAKE_SOURCE_DIR}/src)
link_directories(${CMAKE_BINARY_DIR})

add_subdirectory(pybind11)
pybind11_add_module(pyvarays src/main.cpp)
target_link_libraries(pyvarays PRIVATE ${VARAYS_LIBRARY})

execute_process(
    COMMAND ${PYTHON_EXECUTABLE} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())"
    OUTPUT_VARIABLE PYTHON_DIST_PACKAGES
    OUTPUT_STRIP_TRAILING_WHITESPACE
)
install(TARGETS pyvarays LIBRARY DESTINATION ${PYTHON_DIST_PACKAGES})

add_test(NAME test_pyvarays COMMAND bash ${CMAKE_CURRENT_SOURCE_DIR}/tests/test_pyvarays.sh)
