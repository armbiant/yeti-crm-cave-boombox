"""Unit Test for the pyvarays python module created in  main.cpp."""

import numpy as np
import os
import time
import unittest

import pyvarays as pv


class TestAudioSource(unittest.TestCase):
    def test_constructor(self):
        audio_source = pv.AudioSource(0, 'test', np.zeros(3), [0.0], 0.0)
        self.assertIsInstance(audio_source, pv.AudioSource)


class TestVaraysAuralizerParams(unittest.TestCase):
    def test_constructor_defaults(self):
        params = pv.VaraysAuralizerParams()

        self.assertIsInstance(params, pv.VaraysAuralizerParams)
        self.assertEqual(params.num_inputs, 1)
        self.assertEqual(params.num_outputs, 1)
        self.assertEqual(params.convolver_size, 104800)
        self.assertEqual(params.convolver_partition_size, 256)
        self.assertEqual(params.enable_doppler, True)
        self.assertEqual(params.doppler_interpolation_order, 1)
        self.assertEqual(params.doppler_max_velocity, 50.)
        self.assertEqual(params.doppler_speed_of_sound, 343.)

    def test_constructor_no_defaults(self):
        params = pv.VaraysAuralizerParams(
            num_inputs=0,
            num_outputs=0,
            convolver_size=0,
            convolver_partition_size=0,
            enable_doppler=False,
            doppler_interpolation_order=0,
            doppler_max_velocity=0.,
            doppler_speed_of_sound=0.
        )


        self.assertIsInstance(params, pv.VaraysAuralizerParams)
        self.assertEqual(params.num_inputs, 0)
        self.assertEqual(params.num_outputs, 0)
        self.assertEqual(params.convolver_size, 0)
        self.assertEqual(params.convolver_partition_size, 0)
        self.assertEqual(params.enable_doppler, False)
        self.assertEqual(params.doppler_interpolation_order, 0)
        self.assertEqual(params.doppler_max_velocity, 0.)
        self.assertEqual(params.doppler_speed_of_sound, 0.)


class TestVaraysAuralizer(unittest.TestCase):
    def test_constructor(self):
        varays_auralizer = pv.VaraysAuralizer()

        self.assertIsInstance(varays_auralizer, pv.VaraysAuralizer)

        # Default sample rate.
        self.assertEqual(varays_auralizer.get_sample_rate(), 48000)

    def test_set_vectorized_operations(self):
        varays_auralizer = pv.VaraysAuralizer()

        res = varays_auralizer.set_vectorized_operations(True)

        self.assertIsNone(res)


class TestSoundEventConfig(unittest.TestCase):
    def test_constructor_defaults(self):
        config = pv.SoundEventConfig()

        self.assertIsInstance(config, pv.SoundEventConfig)
        self.assertEqual(config.source_id, 0)
        self.assertEqual(config.length, 0)
        self.assertEqual(config.frequencies, [])
        self.assertEqual(config.energies, [])
        self.assertTrue( (config.spherical_coordinates == np.array([0., 0., 0.])).all() )
        self.assertEqual(config.reflections, [])

    def test_constructor_no_defaults(self):
        config = pv.SoundEventConfig(
            source_id=1,
            length=1,
            frequencies=[0],
            energies=[3,4],
            spherical_coordinates=np.array([1., 1., 1.]),
            reflections=[pv.Reflection.SPECULAR, pv.Reflection.TRANSMISSION]
        )

        self.assertEqual(config.source_id, 1)
        self.assertEqual(config.length, 1)
        self.assertEqual(config.frequencies, [0])
        self.assertEqual(config.energies, [3, 4])
        self.assertTrue( (config.spherical_coordinates == np.array([1., 1., 1.])).all() )
        self.assertEqual(config.reflections, [pv.Reflection.SPECULAR, pv.Reflection.TRANSMISSION])


class TestSoundEvent(unittest.TestCase):
    def test_constructor_defaults(self):
        sound_event = pv.SoundEvent(pv.SoundEventConfig())

        self.assertIsInstance(sound_event, pv.SoundEvent)
        self.assertEqual(sound_event.source_id, 0)
        self.assertEqual(sound_event.length, 0)
        self.assertEqual(sound_event.frequencies, [])
        self.assertEqual(sound_event.energies, [])
        self.assertTrue( (sound_event.spherical_coordinates == np.array([0., 0., 0.])).all() )
        self.assertEqual(sound_event.reflections, [])

    def test_constructor_no_defaults(self):
        config = pv.SoundEventConfig(
            source_id=1,
            length=1,
            frequencies=[0],
            energies=[3,4],
            spherical_coordinates=np.array([1., 1., 1.]),
            reflections=[pv.Reflection.SPECULAR, pv.Reflection.TRANSMISSION]
        )
        sound_event = pv.SoundEvent(config)

        self.assertEqual(sound_event.source_id, 1)
        self.assertEqual(sound_event.length, 1)
        self.assertEqual(sound_event.frequencies, [0])
        self.assertEqual(sound_event.energies, [3, 4])
        self.assertTrue(
            (sound_event.spherical_coordinates == np.array([1., 1., 1.])).all()
        )
        self.assertEqual(
            sound_event.reflections,
            [pv.Reflection.SPECULAR, pv.Reflection.TRANSMISSION]
        )


class TestVaraysContext(unittest.TestCase):
    def setUp(self):
        audio_source = pv.AudioSource(
            0,
            'test',
            np.zeros(3),
            [0.0],
            0.0
        )

        self.varays_context = pv.VaraysContext(
            'test',
            np.zeros(3),
            True,
            [audio_source]
        )

    def test_constructor(self):
        self.assertIsInstance(self.varays_context, pv.VaraysContext)

    def test_add_geom(self):
        vertices = np.zeros((1,3))
        indices = [0] * 37

        geom_id = self.varays_context.add_geom(vertices, indices, True, "test")

        self.assertIsInstance(geom_id, int)

    def test_read_obj_file_returns_false(self):
        """Test read_obj_file returns false if the file does not exist."""
        res = self.varays_context.read_obj_file('./fake_file', True)

        self.assertFalse(res)

    def test_read_obj_file_returns_true(self):
        """Test read_obj_file returns true if the file does exist."""
        filepath = os.path.realpath(os.path.join(
            os.path.dirname(__file__),
            "../../../tools/res/cube.obj"
        ))

        res = self.varays_context.read_obj_file(filepath, True)

        self.assertTrue(res)


class TestVaraysEncoder(unittest.TestCase):
    def test_constructor(self):
        varays_encoder = pv.VaraysEncoder([0.0], 0)

        self.assertIsInstance(varays_encoder, pv.VaraysEncoder)

    def test_set_atmospheric_conditions(self):
        varays_encoder = pv.VaraysEncoder([0.0], 0)

        varays_encoder.set_atmospheric_conditions(0.0, 0.0, 0)

    def test_set_sample_rate(self):
        varays_encoder = pv.VaraysEncoder([0.0], 0)

        varays_encoder.set_sample_rate(0)
        

if __name__ == "__main__":
    unittest.main()
